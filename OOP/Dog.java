package HomeWork.OOP;

/**
 * Created by Ксю on 08.10.2014.
 */
public class Dog  extends Pet {

    int height; //

        // конструктор

        public Dog (String a, int x, int y, int z){
            super (a, x, y);
           height = z;
        }

        void play (){
            System.out.println("I like to play with a ball!");
        }

        @Override
        void say (){
            System.out.println("Гав-гав");
        }

        @Override
        void eat(){
            System.out.println("I am eating Chappi");
        }

        @Override
        void move (){
            System.out.println("I like to run!");
        }

        @Override
        public String toString (){
            return super.toString() + " Height is " + height;
        }
    }

