package HomeWork.OOP;

/**
 * Created by Ксю on 09.10.2014.
 */
public class Bird extends Pet {
    String colorFeather; // цвет перьев

    // конструктор

    public Bird(String a, int x, int y, String d) {
        super(a, x, y);
        colorFeather = d;
    }

    void sing() {
        System.out.println("I can sing a song");
    }

    @Override
    void say() {
        System.out.println("Чирик-чирик!");
    }

    @Override
    void eat() {
        System.out.println("I am eating grains");
    }

    @Override
    void move() {
        System.out.println("I believe I can fly");
    }

    @Override
    public String toString() {
        return super.toString() + " ColorFeather is " + colorFeather;
    }
}


