package HomeWork.OOP;

/**
 * Created by Ксю on 08.10.2014.
 */

public abstract class Pet {

    // переменные

    protected String name;
    protected int age;
    protected int weight;

    // геттеры, сеттеры

    public Pet(String s, int a, int w) {
        name = s;
        age = a;
        weight = w;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    // конструктор

    public void setWeight(int weight) {
        this.weight = weight;
    }

    // методы

    abstract void eat();

    abstract void move();

    abstract void say();


    public String toString() {
        return "Name is " + getName() + ";" + " Age is " + getAge() + ";" + " Weight is " + getWeight();
    }
}



