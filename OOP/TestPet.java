package HomeWork.OOP;

/**
 * Created by Ксю on 09.10.2014.
 */
public class TestPet {
    public static void main(String[] args) {

    // создаем объекты

    Cat cat = new Cat ("Vaska", 2, 6, "siamskiy kot");
        System.out.println (cat.toString());
      cat.eat();
      cat.say();
      cat.catchMouse();
      cat.move();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");

    Dog dog = new Dog ("Chapka", 4, 12, 56);
        System.out.println(dog.toString());
        dog.move();
        dog.eat();
        dog.play();
        dog.say();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");

    Bird bird = new Bird ("Sunny", 1, 1, "brown");
        System.out.println(bird.toString());
        bird.eat();
        bird.sing();
        bird.move();
        bird.say();

        System.out.println("~~~~~~~~~~~~~~~~~~~~~");


    cat.setAge(12);
        System.out.println("New Vaska's age is " + cat.getAge());

    bird.setName("Kesha");
        System.out.println("Now my name is  " + bird.getName());

        System.out.println("My height is " + dog.height);
        dog.height = 34;
        System.out.println("And now my height is " + dog.height);
    }
}
