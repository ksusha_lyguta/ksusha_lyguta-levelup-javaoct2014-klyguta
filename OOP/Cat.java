package HomeWork.OOP;

/**
 * Created by Ксю on 08.10.2014.
 */
    public class Cat extends Pet {
        String breed; // порода

        // конструктор

       public Cat (String a, int x, int y, String m){
        super (a, x, y);
            breed = m;
    }

    void catchMouse (){
        System.out.println("I am catching a mouse!");
    }

    @Override
    void say (){
        System.out.println("Mяу-мяу!");
}

    @Override
    void eat(){
        System.out.println("I am eating Whiskas");
    }

    @Override
    void move (){
        System.out.println("I like to sleep and do murrr-murrr");
    }

    @Override
    public String toString (){
       return super.toString() + " Breed is " + breed;
    }
}
