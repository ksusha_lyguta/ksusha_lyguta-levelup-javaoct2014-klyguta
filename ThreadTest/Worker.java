package HomeWork.ThreadTest;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Ксю on 13.12.2014.
 */
public class Worker implements Runnable{


    private LinkedList<String> storeItems;
    private Object lock;

    public Worker (LinkedList<String> storeItems, Object lock){
        this.storeItems = storeItems;
        this.lock = lock;
    }

    public void construct() {
    synchronized (storeItems){
        if (storeItems.size()>0){
            String item = storeItems.pop();
            System.out.println(item + "processed by" + Thread.currentThread().getName() );
        } else {
            try {
                System.out.println(Thread.currentThread().getName() + "waiting for items");
                lock.wait();
                System.out.println(Thread.currentThread().getName() + "wake up");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    


    @Override
    public void run() {

            while (true){

            synchronized (storeitem){
            construct();
        }

    }
}

