package HomeWork.ThreadTest;

/**
 * Created by Ксю on 13.12.2014.
 */
public class SyncronizedTest {
    public static Object lock = new Object();

    public static void main(String[] args) {
        SyncronizedTest test = new SyncronizedTest();
        test.test();
    }

    public void test() {
        Worker worker1 = new Worker();
        Worker worker2 = new Worker();

        try {
            worker2.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        worker2.notify();
        worker1.notifyAll();

        Thread t1 = new Thread(new TestThread(worker1), "Thread 1");
        Thread t2 = new Thread(new TestThread(worker1), "Thread 2");
        Thread t3 = new Thread(new TestThread(worker2), "Thread 3");
        Thread t4 = new Thread(new TestThread(worker2), "Thread 4");
        Thread t5 = new Thread(new TestThread(worker2), "Thread 5");

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }

        class Worker {
            public synchronized void doSomething(String threadName) {
                //synchronized (SyncronizedTest.lock) {
                System.out.println("Called doSomething from" + threadName);
            }
        }

    public class TestThread implements Runnable {

        private Worker worker;

        public TestThread (Worker worker){

            this.worker = worker;
        }
        @Override
        public void run() {
         for (int i = 0; i<5; i++){
           worker.doSomething(Thread.currentThread().getName());
             try {
                 worker.wait();
             } catch (InterruptedException e) {
                 e.printStackTrace();
             }
         }

        }
    }
}


