package HomeWork.ThreadTest;

import java.util.LinkedList;

/**
 * Created by Ксю on 13.12.2014.
 */
public class Factory {

    private LinkedList<String> storeItems = new LinkedList<>();
    private final Object lock = new Object();

    public static void main(String[] args) {

        Factory factory = new Factory();
        factory.startWork();
    }

    public void startWork(){
        storeItems.add("Item1");
        storeItems.add("Item2");
        storeItems.add("Item3");
        storeItems.add("Item4");
        storeItems.add("Item5");

    Thread storeman = new Thread(new StoreMan(storeItems, lock));
    Thread worker1 = new Thread(new Worker(storeItems, lock), "worker1");
    Thread worker2 = new Thread(new Worker(storeItems, lock), "worker2");
    Thread worker3 = new Thread(new Worker(storeItems, lock), "worker3");

        storeman.start();
        worker1.start();
        worker2.start();
        worker3.start();

    }
}


