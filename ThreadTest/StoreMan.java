package HomeWork.ThreadTest;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by Ксю on 13.12.2014.
 */
public class StoreMan implements Runnable{
    private LinkedList<String> storeItems = new LinkedList<>();
    private Object lock = new Object();

    public StoreMan (LinkedList<String> storeItems,Object lock){
     this.storeItems = storeItems;
        this.lock = lock;
    }

    @Override
    public void run() {

        while (true){
            synchronized (storeItems) {
                storeItems.add("Item");
            }
            lock.notify();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
