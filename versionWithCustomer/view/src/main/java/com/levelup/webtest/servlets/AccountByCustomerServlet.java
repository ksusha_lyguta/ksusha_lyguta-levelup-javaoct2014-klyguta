package com.levelup.webtest.servlets;

import com.levelup.model.Account;
import com.levelup.service.AccountService;
import com.levelup.service.impl.AccountServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ksu on 26.02.2015.
 */
public class AccountByCustomerServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String firstNameOfCustomer = request.getParameter ("firstNameOfCustomer");
        AccountService accountService = new AccountServiceImpl();
        accountService.getAccountByCustomer("firstNameOfCustomer");
        response.sendRedirect("customer.jsp");
    }
}

