<%@ page import="com.levelup.service.AccountService" %>
<%@ page import="com.levelup.service.impl.AccountServiceImpl" %>
<%@ page import="com.levelup.model.Account" %>
<%@ page import="java.util.List" %>

<%--
  Created by IntelliJ IDEA.
  User: Ksu
  Date: 26.02.2015
  Time: 2:13
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>

<%
    AccountService accountService = new AccountServiceImpl();
    List<Account> accountsByCustomer = accountService.getAccountByCustomer("firstName");
    request.setAttribute("accountsByCustomer", accountsByCustomer);

%>

<head>
    <title>List of Accounts by Customer</title>
</head>

<body>

<c:forEach var="accounts" items="${requestScope.accountsByCustomer}">
  </c:forEach>

</body>
</html>
