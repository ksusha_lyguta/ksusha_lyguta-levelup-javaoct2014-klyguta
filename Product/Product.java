package HomeWork.Product;

import HomeWork.Product.Category;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class Product {

    public static final String INSERT_PRODUCT = "INSERT INTO product(nameProduct,categoryId) VALUES(?,?)";

    private Long id;
    private String nameProduct;
    private Long categoryId;

    private Category category;

    public Product() {
    }

    public Product(Long id, String nameProduct, Long categoryId) {
        this.id = id;
        this.nameProduct = nameProduct;
        this.categoryId = categoryId;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
