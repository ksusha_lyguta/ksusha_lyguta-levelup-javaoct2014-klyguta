package HomeWork.Product;

import java.util.Date;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class Category {

    public static final String INSERT_CATEGORY = "INSERT INTO category(nameCategory) VALUES(?)";
    private Long id;
    private String nameCategory;


    public Category() {
    }

    public Category(Long id, String nameCategory) {
        this.id = id;
        this.nameCategory = nameCategory;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

}
