package HomeWork.Product;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class DBManager {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/levelup";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "vereschak83";
    private static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";

    private static DBManager instance;


    private DBManager() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/levelup", "root", "vereschak83");
        return connection;
    }
}

