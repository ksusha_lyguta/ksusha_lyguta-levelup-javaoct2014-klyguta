package HomeWork.Product;

import HomeWork.DBEngine.DBManager;
import HomeWork.Product.*;

import java.sql.*;

/**
 * Created by denis_zavadsky on 12/6/14.
 */
public class ProductController {

    private final static String SELECT_PRODUCT_QUERY = "SELECT  id, nameCategory, id, nameProduct, categoryId,\n"
            + "FROM Product JOIN Category ON (Category.id = Product.categoryId)\n";

    private DBManager dbManager = DBManager.getInstance();
    private Connection connection;
    private PreparedStatement productInsertStatement;
    private PreparedStatement categoryInsertStatement;
    private PreparedStatement productSelectStatement;

    public ProductController() {
        try {
            connection = dbManager.getConnection();
            productInsertStatement = connection.prepareStatement(Product.INSERT_PRODUCT, Statement.RETURN_GENERATED_KEYS);
            categoryInsertStatement = connection.prepareStatement(Category.INSERT_CATEGORY, Statement.RETURN_GENERATED_KEYS);
            productSelectStatement = connection.prepareStatement(SELECT_PRODUCT_QUERY);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public Product createProduct(String nameProduct, String nameCategory) {
        Product product = new Product();
        product.setNameProduct(nameProduct);

        Category category = new Category();
        category.setNameCategory(nameCategory);

        try {
            Long idCategory = saveCategory(category);
            product.setCategoryId(idCategory);

            Long productId = saveProduct(product);
            product.setId(productId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }


    private Long saveCategory(Category category) throws SQLException {
        Long id = null;
        categoryInsertStatement.setString(1, category.getNameCategory());
        categoryInsertStatement.executeUpdate();

        ResultSet resultSet = categoryInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    private Long saveProduct(Product product) throws SQLException {
        Long id = null;
        productInsertStatement.setString(1, product.getNameProduct());
        productInsertStatement.setLong(2, product.getCategoryId());
        productInsertStatement.executeUpdate();

        ResultSet resultSet = productInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    public Product getProductByCategory(String nameCategory) {
        Product product = null;
        Category category;
        try {
            productSelectStatement.setString(1, nameCategory);

            ResultSet resultSet = productSelectStatement.executeQuery();
            if (resultSet.next()) {
                category = new Category(resultSet.getLong("IdCategory"), resultSet.getString("NameCategory"));
                product = new Product(resultSet.getLong("IdProduct"), resultSet.getString("NameProduct"), resultSet.getLong("CategoryName"));
                product.setCategory(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }


    public void close() {
        try {
            productInsertStatement.close();
            categoryInsertStatement.close();
            productSelectStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
