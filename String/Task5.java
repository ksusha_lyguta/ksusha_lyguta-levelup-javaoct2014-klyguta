package HomeWork.String;

/**
 * Created by Ксю on 12.10.2014.
 */
public class Task5 {
    public static void main(String[] args) {
        String s1 = "xHix";
        String s2 = "xHi";
        String s3 = "Hxix";
        String s4 = "Hello";
        System.out.println(withoutX(s1));
        System.out.println(withoutX(s2));
        System.out.println(withoutX(s3));
        System.out.println(withoutX(s4));

    }

    static String withoutX(String s) {


            if (s.startsWith("x")) {
                s= s.substring(1);
            }
            if (s.endsWith("x")) {
                s= s.substring(0, s.length() - 1);
            }
             return s;
        }
    }


