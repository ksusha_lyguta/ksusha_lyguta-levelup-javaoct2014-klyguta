package HomeWork.String;

/**
 * Created by Ксю on 12.10.2014.
 */
public class Task6 {
    public static void main(String[] args) {
        String s1 = "cite";
        String s2 = "Yay";
        String s3 = "i";
        String s4 = "Hello";
        String s5 = "cite";
        String s6 = "Yay";
        System.out.println(makeTags(s1,s2));
        System.out.println(makeTags(s3,s4));
        System.out.println(makeTags(s5,s6));
    }

    static String makeTags(String k, String n) {
        String tag1 = "<" + k + ">";
        String tag2 = "<" + k + "/>";
        return tag1 + n + tag2;
    }
}
