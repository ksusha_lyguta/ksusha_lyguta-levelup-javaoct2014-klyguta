package HomeWork.String;

/**
 * Created by Ксю on 12.10.2014.
 */
public class Task4 {
    public static void main(String[] args) {

        String s1 = "badxx";
        String s2 = "xxbadxx";
        String s3 = "xbadxx";
        System.out.println(hasBad(s1));
        System.out.println(hasBad(s2));
        System.out.println(hasBad(s3));
    }

    static boolean hasBad(String s) {
        if (s.startsWith("bad")||s.substring(1).startsWith("bad")) {
            return true;
        } else return false;
    }
}