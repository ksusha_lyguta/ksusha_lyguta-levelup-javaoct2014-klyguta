package HomeWork.String;

/**
 * Created by Ксю on 11.10.2014.
 */
public class Task2 {
    public static void main(String[] args) {
        String s = "HelloThere";
        System.out.println(firstHalf(s));
    }

    static String firstHalf(String s) {
        return s.substring(0, s.length() / 2);
    }
}


