package HomeWork.String;

/**
 * Created by Ксю on 12.10.2014.
 */
public class Task7 {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = "hi";
        String s3 = "h";
        System.out.println(atFirst(s1));
        System.out.println(atFirst(s2));
        System.out.println(atFirst(s3));
    }
    static String atFirst(String s){
        if (s.length()<2){
            return s.charAt(0)+"@";
        } else {
            return s.substring(0,2);
        }
    }
}
