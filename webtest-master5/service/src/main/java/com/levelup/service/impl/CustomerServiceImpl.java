package com.levelup.service.impl;

import com.levelup.dao.AccountRepository;
import com.levelup.dao.BankRepository;
import com.levelup.dao.CustomerRepository;
import com.levelup.dao.impl.AccountRepositoryImpl;
import com.levelup.dao.impl.BankRepositoryImpl;
import com.levelup.dao.impl.CustomerRepositoryImpl;
import com.levelup.model.Customer;
import com.levelup.service.CustomerService;

import java.util.Date;
import java.util.List;

/**
 * Created by Ksu on 24.02.2015.
 */
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository = new CustomerRepositoryImpl();

    @Override
    public Customer getCustomerById(Long id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public Customer getCustomerByFirstName(String customerFirstName) {
        return customerRepository.getCustomerByFirstName(customerFirstName);
    }

    @Override
    public Customer getCustomerByLastName(String customerLastName) {
        return customerRepository.getCustomerByLastName(customerLastName);
    }

    @Override
    public Customer getCustomerByBirthDate(Date customerBirthDate) {
        return customerRepository.getCustomerByBirthDate(customerBirthDate);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @Override
    public void createCustomer(Customer customer) {
       customerRepository.createCustomer(customer);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customerRepository.updateCustomer(customer);
    }

    @Override
    public void deleteCustomer(Customer customer) {
        customerRepository.deleteCustomer(customer);
    }
}
