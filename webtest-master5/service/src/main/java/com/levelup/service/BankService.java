package com.levelup.service;

import com.levelup.model.Bank;

import java.util.List;

/**
 * Created by Ksu on 24.02.2015.
 */
public interface BankService {

    Bank getBankById(Long id);

    Bank getBankByName(String bankName);

    List<Bank> getAllBanks();


    void createBank (Bank bank);

    void updateBank (Bank bank);

    void deleteBank (Bank bank);
}
