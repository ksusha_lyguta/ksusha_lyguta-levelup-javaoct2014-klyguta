package com.levelup.service;

import com.levelup.model.Customer;

import java.util.Date;
import java.util.List;

/**
 * Created by Ksu on 24.02.2015.
 */
public interface CustomerService {

    Customer getCustomerById(Long id);

    Customer getCustomerByFirstName(String customerFirstName);

    Customer getCustomerByLastName(String customerLastName);

    Customer getCustomerByBirthDate (Date customerBirthDate);

    List<Customer> getAllCustomers();


    void createCustomer(Customer customer);

    void updateCustomer(Customer customer);

    void deleteCustomer(Customer customer);


}
