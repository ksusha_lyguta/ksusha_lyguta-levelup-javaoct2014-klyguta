package com.levelup.service.impl;

import com.levelup.dao.AccountRepository;
import com.levelup.dao.BankRepository;
import com.levelup.dao.CustomerRepository;
import com.levelup.dao.impl.AccountRepositoryImpl;
import com.levelup.dao.impl.BankRepositoryImpl;
import com.levelup.dao.impl.CustomerRepositoryImpl;
import com.levelup.model.Bank;
import com.levelup.model.Customer;
import com.levelup.service.BankService;

import java.util.List;

/**
 * Created by Ksu on 24.02.2015.
 */
public class BankServiceImpl implements BankService {

    private BankRepository bankRepository = new BankRepositoryImpl();

    private AccountRepository accountRepository = new AccountRepositoryImpl();

    private CustomerRepository customerRepository = new CustomerRepositoryImpl() ;

    @Override
    public Bank getBankById(Long id) {
        return bankRepository.getBankById(id);
    }

    @Override
    public Bank getBankByName(String bankName) {
        return bankRepository.getBankByName(bankName);
    }

    @Override
    public List<Bank> getAllBanks() {
        return bankRepository.getAllBanks();
    }

    @Override
    public void createBank(Bank bank) {
      bankRepository.createBank(bank);

    }

    @Override
    public void updateBank(Bank bank) {
    bankRepository.updateBank(bank);
    }

    @Override
    public void deleteBank(Bank bank) {
     bankRepository.deleteBank(bank);
    }
}


