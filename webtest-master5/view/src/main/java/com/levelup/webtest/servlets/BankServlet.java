package com.levelup.webtest.servlets;

import com.levelup.model.Account;
import com.levelup.model.Bank;
import com.levelup.model.Customer;
import com.levelup.service.BankService;
import com.levelup.service.impl.BankServiceImpl;
import com.levelup.webtest.Constants;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ksu on 24.02.2015.
 */
public class BankServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Bank bank = getBank(req);
        BankService bankService = new BankServiceImpl();
        bankService.createBank(bank);
        bankService.updateBank(bank);
        bankService.createBank(bank);
    }

    private Bank getBank(HttpServletRequest request) {
        Bank bank = new Bank();
        Account account = new Account();
        Customer customer = new Customer();

        bank.setName(request.getParameter(Constants.BANK_NAME_PARAMETER));

        account.setAccountNumber(Constants.ACCOUNT_NUMBER_PARAMETER);
        account.setBalance(Float.parseFloat(request.getParameter(Constants.ACCOUNT_BALANCE_PARAMETER)));

        customer.setFirstName(request.getParameter(Constants.FIRST_NAME_PARAMETER));
        customer.setLastName(request.getParameter(Constants.LAST_NAME_PARAMETER));


        return bank;
    }
}
