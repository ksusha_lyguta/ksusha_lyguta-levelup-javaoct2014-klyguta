package com.levelup.webtest.servlets;

import com.levelup.model.Customer;
import com.levelup.service.CustomerService;
import com.levelup.service.impl.CustomerServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ksu on 24.02.2015.
 */
public class CustomerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Customer customer = getCustomer (req);
        CustomerService customerService = new CustomerServiceImpl();
        customerService.createCustomer(customer);
        customerService.updateCustomer(customer);
        customerService.deleteCustomer(customer);
    }
}
