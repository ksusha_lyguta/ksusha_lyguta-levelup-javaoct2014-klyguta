package com.levelup.dao;


import com.levelup.model.Bank;

import java.util.List;

/**
 * Created by denis_zavadsky on 2/21/15.
 */
public interface BankRepository {

    void createBank(Bank bank);

    void updateBank(Bank bank);

    void deleteBank(Bank bank);


    Bank getBankById(Long id);

    Bank getBankByName(String accountName);

    List<Bank> getAllBanks();
}
