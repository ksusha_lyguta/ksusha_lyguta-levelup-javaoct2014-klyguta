package com.levelup.dao;

import com.levelup.model.Customer;

import java.util.Date;
import java.util.List;

/**
 * Created by denis_zavadsky on 2/21/15.
 */
public interface CustomerRepository {

    void createCustomer(Customer customer);
    void updateCustomer(Customer customer);
    void deleteCustomer(Customer customer);

    Customer getCustomerById(Long id);
    Customer getCustomerByFirstName(String customerFirstName);

    Customer getCustomerByLastName(String customerLastName);

    Customer getCustomerByBirthDate (Date customerBirthDate);

    List<Customer> getAllCustomers();
}
