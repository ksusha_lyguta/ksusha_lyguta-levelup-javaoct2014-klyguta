package com.levelup.dao;

import com.levelup.model.Transaction;

import java.util.Date;
import java.util.List;

/**
 * Created by Ksu on 24.02.2015.
 */
public interface TransactionRepository {
    Transaction getTransactionById(Long id);

    Transaction getTransactionByAmount(float transactionAmount);

    Transaction getTransactionByDate(Date transactionDate);


    List<Transaction> getAllTransactions();


    void createTransaction(Transaction transaction);

    void updateTransction(Transaction transaction);

    void deleteTransaction(Transaction transaction);

}
