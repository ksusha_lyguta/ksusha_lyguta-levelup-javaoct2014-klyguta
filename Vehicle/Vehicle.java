package HomeWork.Vehicle;

/**
 * Created by Ксю on 18.10.2014.
 */
public abstract class Vehicle {

    protected int weight;
    protected int height;
    protected int width;

    protected Vehicle() {
    }

    protected Vehicle(int weight, int height, int width) {
        this.weight = weight;
        this.height = height;
        this.width = width;
    }


    abstract void aging();
    abstract void move ();

    public int getWeight() {
        return weight;
    }

    public void setWeight(double weight1) {
        if (weight1 > 100.78) {
            System.out.println("Can't have such a big mass");
        } else {
            System.out.println("Weight is " + weight1);
        }
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(float width1) {
        if (width1 > 50.34) {
            System.out.println("Can't have such a big weight");
        } else {
            System.out.println("Width is " + width1);
        }
    }
}








