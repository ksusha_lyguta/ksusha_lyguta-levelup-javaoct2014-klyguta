package HomeWork.Vehicle;

/**
 * Created by Ксю on 18.10.2014.
 */
public class Moto extends Vehicle {

    public int motoAge;
    public int motoSpeed;

    @Override
    void aging() {
        System.out.println("Age of Moto is " + motoAge + " years");

    }

    @Override
    void move() {

    }

    public void ride(int motoSpeed) {

        System.out.println("Moto speed is " + this.motoSpeed);
    }

    public void ride(double speed) {
        System.out.println("Moto speed is " + speed);
    }
}
