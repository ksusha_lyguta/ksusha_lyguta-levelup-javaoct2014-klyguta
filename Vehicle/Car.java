package HomeWork.Vehicle;

/**
 * Created by Ксю on 18.10.2014.
 */
public class Car extends Vehicle implements Movable {
    int carSpeed;
    int carAge;

    @Override
    void aging() {
        System.out.println("This car is" + carAge + "years old");
    }

    public void addPassenger(int passenger) {
        System.out.println("Car has " + passenger + " passengers");
    }

    @Override
    public void move() {

    }

    public void doorCount(int door) {
        System.out.println("Number of doors is " + door);
    }

}
