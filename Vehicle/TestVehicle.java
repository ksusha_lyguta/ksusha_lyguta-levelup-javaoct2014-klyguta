package HomeWork.Vehicle;

/**
 * Created by Ксю on 18.10.2014.
 */
public class TestVehicle {
    public static void main(String[] args) {
        Vehicle car = new Car();
        Vehicle truck = new Truck();
        Vehicle moto = new Moto();

        ((Car) car).addPassenger(5);
        truck.setWeight(200.6);
        truck.setWeight(23);
        System.out.println("My weight is  " + truck.getWeight() + " kg");
        ((Moto) moto).ride(50.7);
        moto.aging();
        car.setHeight(34);
        System.out.println(car.getHeight());

        Vehicle[] vehicles = {new Car(), new Moto(), new Truck()};
        int [] lats = {1, 38, 52, 43, 10};
        int [] lngs = {2, 38, 65, 24, 12};

        for (int i = 0; i < vehicles.length; i++) {
            Vehicle v = vehicles[i];
            v.move();
            if (v instanceof Car){
                ((Car) v).addPassenger(8);
            }
            if (v instanceof Moto){
                ((Moto)v).ride(67.8);
            }
            if (v instanceof Truck){
                ((Truck)v).changePart();
            }
        }
    }
}

