package com.levelup.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * Created by Ksu on 01.03.2015.
 */
@Entity
@Table(name = "TEACHER")
public class Teacher extends HumanObject {

    private Long id;

    private String firstName;

    private String lastName;

    private Set<Student> students;

    private Date birthDate;

    public Teacher() {

    }


    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "firstName", nullable = false, length = 38)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "birthDate")
    @Temporal(value = TemporalType.DATE)
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Column(name = "lastName", nullable = false, length = 38)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "teacher")
    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }
}

