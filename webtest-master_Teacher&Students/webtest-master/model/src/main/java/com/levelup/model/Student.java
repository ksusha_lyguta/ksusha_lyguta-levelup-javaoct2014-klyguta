package com.levelup.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Ksu on 01.03.2015.
 */
@Entity
@Table(name = "STUDENT")
public class Student extends HumanObject {

    private Long id;

    private String firstName;

    private String lastName;

    private Teacher teacher;

    private Date birthDate;

    public Student() {

    }


    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "firstName", nullable = false, length = 38)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "lastName", nullable = false, length = 38)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @Column(name = "birthDate")
    @Temporal(value = TemporalType.DATE)
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id")
    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}