package com.levelup.webtest.servlets;

import com.levelup.model.Student;
import com.levelup.model.Teacher;
import com.levelup.service.StudentService;
import com.levelup.service.TeacherService;
import com.levelup.service.impl.StudentServiceImpl;
import com.levelup.service.impl.TeacherServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ksu on 05.03.2015.
 */
public class TeacherServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doPost(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Teacher teacher = new Teacher();
        Student student = new Student();

        TeacherService teacherService = new TeacherServiceImpl();
        StudentService studentService = new StudentServiceImpl();
        teacherService.createTeacher(teacher);


        teacher.setFirstName("firstName");
        teacher.setLastName("lastName");
        String teacherBirhDate = request.getParameter("birhDate");
        request.setAttribute("birthDate", teacherBirhDate);

        String id = request.getParameter("id");
        List<Student> students = studentService.getStudentByTeacherId(Long.parseLong(id));
        request.setAttribute("students", students);

    }
}
