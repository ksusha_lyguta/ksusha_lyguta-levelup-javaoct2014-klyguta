package com.levelup.webtest.servlets;

import com.levelup.model.Student;
import com.levelup.model.Teacher;
import com.levelup.service.StudentService;
import com.levelup.service.impl.StudentServiceImpl;
import sun.util.calendar.LocalGregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Ksu on 05.03.2015.
 */
public class StudentServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Student student = new Student();
        Teacher teacher = new Teacher();

        StudentService studentService = new StudentServiceImpl();
        studentService.createStudent(student);

        student.setFirstName("firstName");
        student.setLastName("lastName");
        String studentBirthDate = request.getParameter("birhDate");
        request.setAttribute("birthDate", studentBirthDate);

        student.setTeacher(teacher);

        request.getSession().setAttribute("objectId", student.getId());
        request.getRequestDispatcher("/showStudent.jsp");

   }
}
