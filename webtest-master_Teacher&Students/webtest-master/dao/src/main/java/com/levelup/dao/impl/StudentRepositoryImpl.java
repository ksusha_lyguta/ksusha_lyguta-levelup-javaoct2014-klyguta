package com.levelup.dao.impl;

import com.levelup.dao.HibernateSessionProvider;
import com.levelup.dao.StudentRepository;
import com.levelup.model.Student;

import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Ksu on 03.03.2015.
 */
public class StudentRepositoryImpl implements StudentRepository {
    @Override
    public void createStudent(Student student) {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        session.persist(student);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Student getStudentByFirstName(String firstName) {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Student student = (Student) session.get(Student.class, firstName);
        session.getTransaction().commit();
        session.close();
        return student;
    }

    @Override
    public Student getStudentByLastName(String lastName) {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Student student = (Student) session.get(Student.class, lastName);
        session.getTransaction().commit();
        session.close();
        return student;
    }

    @Override
    public List<Student> getStudentByTeacherId(Long id) {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Student as a where a.teacher.id = :id");
        query.setParameter("id",id);
        List <Student> res = query.list();
        session.getTransaction().commit();
        session.close();
        return res;
    }

    @Override
    public List<Student> getStudentByNames(String firstName, String lastName) {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Student as a where a.firstName = :firstName " +
                "and a.lastName = :lastName");
        query.setParameter("firstName", firstName +"%");
        query.setParameter("lastName", lastName + "%");
        List<Student> students = query.list();
        session.getTransaction().commit();
        session.close();
        return students;
    }
}
