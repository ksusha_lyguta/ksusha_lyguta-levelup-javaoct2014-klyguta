package com.levelup.dao;

import com.levelup.model.Student;

import java.util.List;

/**
 * Created by Ksu on 03.03.2015.
 */
public interface StudentRepository {

    public void createStudent(Student student);

    Student getStudentByFirstName(String firstName);

    Student getStudentByLastName (String lastName);

    List<Student> getStudentByTeacherId (Long id);

    List <Student> getStudentByNames (String firstName, String lastName);
}
