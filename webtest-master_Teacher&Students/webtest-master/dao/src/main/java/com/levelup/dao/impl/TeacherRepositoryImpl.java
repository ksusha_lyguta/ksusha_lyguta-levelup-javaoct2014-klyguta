package com.levelup.dao.impl;

import com.levelup.dao.HibernateSessionProvider;
import com.levelup.dao.TeacherRepository;
import com.levelup.model.Teacher;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Ksu on 03.03.2015.
 */
public class TeacherRepositoryImpl implements TeacherRepository {
    @Override
    public void createTeacher(Teacher teacher) {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        session.persist(teacher);
        session.getTransaction().commit();
        session.close();

    }

    @Override
    public Teacher getTeacherByFirstName(String firstName) {
        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Teacher teacher = (Teacher) session.get(Teacher.class, firstName);
        session.getTransaction().commit();
        session.close();
        return teacher;
    }

    @Override
    public Teacher getTeacherByLastName(String lastName) {
        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Teacher teacher = (Teacher) session.get(Teacher.class, lastName);
        session.getTransaction().commit();
        session.close();
        return teacher;
    }

    @Override
    public List<Teacher> getTeacherByNames(String firstName, String lastName) {
        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Teacher as a where a.firstName = :firstName " +
                "and a.lastName = :lastName");
        query.setParameter("firstName", firstName + "%");
        query.setParameter("lastName", lastName + "%");
        List<Teacher> teacherByNames = query.list();
        session.getTransaction().commit();
        session.close();
        return teacherByNames;
    }


    @Override
    public List<Teacher> getAllTeachers() {

        Session session = HibernateSessionProvider.getSession();
        session.beginTransaction();
        Query query = session.createQuery("from Teacher a ");
        List<Teacher> teachers = query.list();
        session.getTransaction().commit();
        session.close();
        return teachers;
    }
}
