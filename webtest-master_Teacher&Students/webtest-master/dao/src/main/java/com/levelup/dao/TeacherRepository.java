package com.levelup.dao;

import com.levelup.model.Teacher;

import java.util.List;

/**
 * Created by Ksu on 03.03.2015.
 */
public interface TeacherRepository {

    public void createTeacher(Teacher teacher);

    Teacher getTeacherByFirstName(String firstName);

    Teacher getTeacherByLastName(String lastName);

    List<Teacher> getTeacherByNames ( String firstName, String lastName);

    List<Teacher> getAllTeachers();

}
