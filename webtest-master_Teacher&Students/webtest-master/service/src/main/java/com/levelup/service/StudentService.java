package com.levelup.service;

import com.levelup.model.Student;

import java.util.List;

/**
 * Created by Ksu on 04.03.2015.
 */
public interface StudentService {

    public void createStudent(Student student);

    Student getStudentByFirstName(String firstName);

    Student getStudentByLastName (String lastName);

    List<Student> getStudentByTeacherId (Long id);

    List <Student> getStudentByNames (String firstName, String lastName);
}
