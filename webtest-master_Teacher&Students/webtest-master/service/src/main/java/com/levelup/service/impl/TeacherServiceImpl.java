package com.levelup.service.impl;

import com.levelup.dao.StudentRepository;
import com.levelup.dao.TeacherRepository;
import com.levelup.dao.impl.StudentRepositoryImpl;
import com.levelup.dao.impl.TeacherRepositoryImpl;
import com.levelup.model.Student;
import com.levelup.model.Teacher;
import com.levelup.service.TeacherService;

import java.util.List;

/**
 * Created by Ksu on 04.03.2015.
 */
public class TeacherServiceImpl implements TeacherService{

    private TeacherRepository teacherRepository = new TeacherRepositoryImpl();

    private StudentRepository studentRepository = new StudentRepositoryImpl();

    @Override
    public void createTeacher(Teacher teacher) {
       teacherRepository.createTeacher(teacher);
    }

    @Override
    public Teacher getTeacherByFirstName(String firstName) {
        return teacherRepository.getTeacherByFirstName(firstName);
    }

    @Override
    public Teacher getTeacherByLastName(String lastName) {
        return teacherRepository.getTeacherByLastName(lastName);
    }

    @Override
    public List<Teacher> getTeacherByNames(String firstName, String lastName) {
        return teacherRepository.getTeacherByNames(firstName, lastName);
    }

    @Override
    public List<Teacher> getAllTeachers() {
        return teacherRepository.getAllTeachers();
    }
}
