package com.levelup.service.impl;

import com.levelup.dao.StudentRepository;
import com.levelup.dao.TeacherRepository;
import com.levelup.dao.impl.StudentRepositoryImpl;
import com.levelup.dao.impl.TeacherRepositoryImpl;
import com.levelup.model.Student;
import com.levelup.model.Teacher;
import com.levelup.service.StudentService;

import java.util.List;

/**
 * Created by Ksu on 04.03.2015.
 */
public class StudentServiceImpl implements StudentService{

    private StudentRepository studentRepository = new StudentRepositoryImpl();

    private TeacherRepository teacherRepository = new TeacherRepositoryImpl();

    @Override
    public void createStudent(Student student) {
         studentRepository.createStudent(student);
    }

    @Override
    public Student getStudentByFirstName(String firstName) {
        return studentRepository.getStudentByFirstName(firstName);
    }

    @Override
    public Student getStudentByLastName(String lastName) {
        return studentRepository.getStudentByLastName(lastName);
    }

    @Override
    public List<Student> getStudentByTeacherId(Long id) {
        return studentRepository.getStudentByTeacherId(id);
    }

    @Override
    public List<Student> getStudentByNames(String firstName, String lastName) {
        return studentRepository.getStudentByNames(firstName, lastName);
    }
}
