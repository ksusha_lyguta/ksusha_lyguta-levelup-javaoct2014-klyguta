$(document).ready(function () {
    $("input[type='checkbox']").on("change", function (event) {
        event.preventDefault();
        $.ajax(
            {
                type: 'POST',
                url: 'index.jsp',
                dataType: 'html',
                data: {
                    firstName: $('#firstName').val(),
                    lastName: $('#lastName').val(),
                    option1: $('#option1').val(),
                    option2: $('#option2').val(),
                    option3: $('#option3').val()
                },
                success: function (data) {
                    $("#option1","#option2","#option3","lastName","firstName").html(data);
                }
            }
        );
    });
});

$("input[type='checkbox']").on("change", function (event) {
    event.preventDefault();
    var selectedCheckbox = event.source;
    var selectedId = selectedCheckbox.id;
    var optionNumber = selectedId.substring(1);
    $.ajax(
        {
            type: "post",
            url: "options.jsp",
            dataType: "html",
            data: {
                firstName: $('#firstName').val(),
                lastName: $('#lastName').val(),
                option: optionNumber
            },
            success: function (data) {
                $("#option" + optionNumber).html(data)
            }
        }
    )
})