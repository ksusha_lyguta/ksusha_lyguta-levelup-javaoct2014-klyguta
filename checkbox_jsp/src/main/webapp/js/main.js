$(document).ready(function(){
$("input[type='checkbox']").on("change", function (event){
        var selectedCheckbox = event.source;
        var selectedId = selectedCheckbox.id;
        var optionNumber= selectedId.substring(1);
        $.ajax(
            {
                type:"post",
                url:"options.jsp",
                dataType:"html",
                data:{
                    firstName: $('#firstName').val(),
                    lastName: $('#lastName').val(),
                    option:optionNumber
                },
                success: function(data){
                    $("#option"+optionNumber).html(data)
                }
            }
        )
    });
})