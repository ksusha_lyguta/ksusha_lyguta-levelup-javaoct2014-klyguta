<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 27.01.15
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>index</title>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="js/form.js"></script>

    <script>
        $(function () {
            var field = new Array("firstName", "lastName");
            $("input[type='checkbox']").on("change", function(){
                var error = 0;
                $("input[type='checkbox']").find(":input").each(function () {
                    for (var i = 0; i < field.length; i++) {
                        if ($(this).attr("name") == field[i]) {
                            if (!$(this).val()) {
                                $(this).css('border', 'red 1px solid');
                                error = 1;
                            }
                        }
                    }
                })
                if (error == 0)
                    return true;
                else {
                    if (error == 1)
                        return false;
                }
            })
        });
    </script>
</head>

<body>

<%
    String firstName = request.getParameter("firstName");
    String lastName = request.getParameter("lastName");

%>
<form action="index.jsp" method="post">

    <h3>Index_form</h3>
    firstName: <input name="firstName" type="text">
    lastName: <input name="lastName" type="text">

    <input id="c1" type="checkbox" name="checkbox1">checkbox1<Br>

    <div id="option1"></div>

    <input id="c2" type="checkbox" name="checkbox2">checkbox2<Br>

    <div id="option2"></div>

    <input id="c3" type="checkbox" name="checkbox3">checkbox3<Br>

    <div id="option3"></div>

</form>


</body>
</html>
