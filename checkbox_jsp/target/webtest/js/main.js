$(document).ready(function(){
$("#Submit").on('click', function(event){
    event.preventDefault();
    $.ajax(
        {
            type:"Post",
            url:"index.jsp",
            dataType:"html",
            data:{
                checkbox1: $('#checkbox1').val(),
                checkbox2: $('#checkbox2').val(),
                checkbox3: $('#checkbox2').val()
            },
            success: function(data){
                $(   ).html(data)
            }
        }
    )
})


    $("input[type='checkbox']").on("change", function (event){
        var selectedCheckbox = event.source;
        var selectedId = selectedCheckbox.id;
        var optionNumber= selectedId.substring(1);
        $.ajax(
            {
                type:"post",
                url:"options.jsp",
                dataType:"html",
                data:{
                    firstName: $('#firstName').val(),
                    lastName: $('#lastName').val(),
                    option:optionNumber
                },
                success: function(data){
                    $("#option"+optionNumber).html(data)
                }
            }
        )
    });
})