<%--
  Created by IntelliJ IDEA.
  User: denis_zavadsky
  Date: 1/17/15
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script  src="js/main.js"></script>
</head>
<body>
<%
    String user = request.getParameter("login");
    String password = request.getParameter("password");
    if (user != null && password != null && !user.isEmpty() && !password.isEmpty()) {
        session.setAttribute("login",user);
        session.setAttribute("password", password);
%>
<form action="login.jsp" method="get">
    <h3>Login_form</h3>
    User: <input type="text" name="user">
    Password: <input type="pass" name="password">
    <input type="submit">
</form>
<%
    } else {
        Boolean loginFalse =false;
        Boolean passwordFalse = false;

        if (user ==null || user.isEmpty()){
            loginFalse = true;
        }
        if (password == null || password.isEmpty()){
            passwordFalse = true;
        }
%>
<form action = "login.jsp">

<div>User = <input id = "user" name = "user" type = "text" <%=(loginFalse!=null&&loginFalse)?" class = \"invalid\"":""%>  </div>
<div> Password = <input id = "password" name = "password" type = "password"
<%=(passwordFalse!=null&&passwordFalse)?"class=\"invalid\"":""%>
                        value="<%=(passwordFalse!=null && passwordFalse)?"":password%>">
</div>
    <input type="button" id = "loginButton" value = "submit">
</form>

<%
    }
    %>
</body>
</html>
