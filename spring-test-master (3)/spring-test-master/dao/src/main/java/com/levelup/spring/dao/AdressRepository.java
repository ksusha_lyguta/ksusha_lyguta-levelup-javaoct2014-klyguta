package com.levelup.spring.dao;

import com.levelup.spring.model.user.Adress;

/**
 * Created by java on 14.03.15.
 */
public interface AdressRepository {

    void createAdress(Adress adress);

    Adress getAdressById(Long id);

    Adress getAdressByStreetAndCity(String street, String city);

    Adress getAdressByHouse (Integer house);

    Adress getAdressByFlat(Integer flat);

}