package com.levelup.spring.dao.impl;


import com.levelup.spring.dao.AdressRepository;
import com.levelup.spring.model.user.Adress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by java on 14.03.15.
 */

@Repository("adressRepository")
public class AdressRepositoryImpl implements AdressRepository {


    private JdbcTemplate jdbcTemplate;
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private SimpleJdbcInsert jdbcInsertAdress;


    @Autowired
    public void setDataSource(javax.sql.DataSource dataSource) {
        this.jdbcInsertAdress = new SimpleJdbcInsert(dataSource).withTableName("ADRESS").usingGeneratedKeyColumns("id");
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }


    @Override
    public void createAdress(Adress adress) {
        SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(adress);
        Number adressId = jdbcInsertAdress.executeAndReturnKey(namedParameters);
        adress.setId(adressId.longValue());
    }

    @Override
    public Adress getAdressById(Long id) {
        String sql = "SELECT * FROM ADRESS where id=:id";
        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("id", id.toString());
        Adress adress = namedParameterJdbcTemplate.queryForObject(sql, parameterMap, new BeanPropertyRowMapper<Adress>(Adress.class));
        return adress;
    }

    @Override
    public Adress getAdressByStreetAndCity(String street, String city) {
        String sql = "SELECT * FROM Adress where street=:street AND city=:city";
        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("street", street);
        parameterMap.put("city", city);
        SqlParameterSource namedParameters = new MapSqlParameterSource(parameterMap);

        Adress adress = namedParameterJdbcTemplate.queryForObject(sql, namedParameters, new BeanPropertyRowMapper<Adress>(Adress.class));
        return adress;
    }

    @Override
    public Adress getAdressByHouse(Integer house) {
        String sql = "SELECT * FROM ADRESS where house:=house";
        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("house", house.toString());
        Adress adress = namedParameterJdbcTemplate.queryForObject(sql, parameterMap, new BeanPropertyRowMapper<Adress>(Adress.class));
        return adress;
    }

    @Override
    public Adress getAdressByFlat(Integer flat) {
        String sql = "SELECT * FROM ADRESS where flat=:flat";
        Map<String, String> parameterMap = new HashMap<>();
        parameterMap.put("flat", flat.toString());
        Adress adress = namedParameterJdbcTemplate.queryForObject(sql, parameterMap, new BeanPropertyRowMapper<Adress>(Adress.class));
        return adress;
    }

}
