package HomeWork.Cars;

/**
 * Created by Ксю on 26.11.2014.
 */
public class Lada {

    @Applicable(details = {"AA1", "AA2",})
     Part part1;
    @Applicable(details = {"BB1","BB2"})
    Part part2;
    @Applicable(details = {"CC1"})
    Part part3;



    public Lada (){ };

    public Lada(Part part1, Part part2, Part part3, String model) {
        this.part1 = part1;
        this.part2 = part2;
        this.part3 = part3;

    }

    public Part getPart1() {
        return part1;
    }

    public void setPart1(Part part1) {
        this.part1 = part1;
    }

    public Part getPart2() {
        return part2;
    }

    public void setPart2(Part part2) {
        this.part2 = part2;
    }

    public Part getPart3() {
        return part3;
    }

    public void setPart3(Part part3) {
        this.part3 = part3;
    }



    @Override
    public String toString() {
        return "Lada{" +
                "part1=" + part1 +
                ", part2=" + part2 +
                ", part3=" + part3 +

                '}';
    }
}



