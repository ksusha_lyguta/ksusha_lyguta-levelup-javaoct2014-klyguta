package HomeWork.Cars;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Ксю on 26.11.2014.
 */

@Target (ElementType.FIELD)
@Retention (RetentionPolicy.RUNTIME)
public @interface Applicable {

    String [] details ();
}
