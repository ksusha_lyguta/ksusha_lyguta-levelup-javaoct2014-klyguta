package HomeWork.Cars;

/**
 * Created by Ксю on 26.11.2014.
 */
public class Kamaz {

    @Applicable(details = {"AA1"} )
     Part sector1;
    @Applicable(details = {"BB1",} )
     Part sector2;
    @Applicable(details = {"CC2"} )
     Part sector3;



   public Kamaz(){};

    public Kamaz(Part sector1, Part sector2, Part sector3, String model) {
        this.sector1 = sector1;
        this.sector2 = sector2;
        this.sector3 = sector3;

    }

    public Part getSector1() {
        return sector1;
    }

    public void setSector1(Part sector1) {
        this.sector1 = sector1;
    }

    public Part getSector2() {
        return sector2;
    }

    public void setSector2(Part sector2) {
        this.sector2 = sector2;
    }

    public Part getSector3() {
        return sector3;
    }

    public void setSector3(Part sector3) {
        this.sector3 = sector3;
    }



    @Override
    public String toString() {
        return "Kamaz{" +
                "sector1=" + sector1 +
                ", sector2=" + sector2 +
                ", sector3=" + sector3 +

                '}';
    }
}





