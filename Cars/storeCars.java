package HomeWork.Cars;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;


/*Есть объекты Part, есть объекты конкретных машинок Toyota, Honda к которых есть такие Part part1, Part part2
        Есть HashMap<String,List<Part>> в которой по ключу (код Part) лежат листы доступных деталек с этим кодом
      Есть аннотация которая говорит что в какое-то поле Part какой-то машинки можно положить детали вот с таким кодом
        Создать три модельных класса автомобилей, которые содержат три поля part1, part2, part3 типа Parts и поле model типа String.

class BMWCar{

    @Applicable(partNumbers = {"1E28", "2C48"})
    Part part1;

    @Applicable(partNumbers = {"3A57", "148C"})
    Part part2;

    @Applicable(partNumbers = {.....})
    Part part3;

    String model;
}

Поля part1, part2, part3 должны быть проаннотированы аннотацией @Applicable, которая содержит в себе массив типа
        String взаимозаменяемых номеров partNumbers.

        Модельный класс Part содержит в себе поле типа String - partNumber

class Part {
    String partNumber;

    Part(){};

    Part(String partNumber) {
        this.partNumber = partNumber;
    }

    Setters/Getters
}


Создать основной класс, который будет содержать в себе HashMap<String, List<Part>> partsInStock, где String - это
        partNumber, а List<Part> - детали этого partNumber в наличии. Организовать процесс сборки автомобилей, номера совместимых
        с данной моделью автомобиля брать из аннотации к данной детали. Если заканчиваются детали одного partNumber, использовать
        совместимые детали. Получение деталей из List<Part> с помощью pop (в LinkedList есть реализация стэка)*/
/**
 * Created by Ксю on 26.11.2014.
 */
public class storeCars {
    public static void main(String[] args) {

        HashMap<String, LinkedList<Part>> storeCar = new HashMap<>();

        LinkedList<Part> AA1 = new LinkedList<>();
        LinkedList<Part> AA2 = new LinkedList<>();
        LinkedList<Part> BB1 = new LinkedList<>();
        LinkedList<Part> BB2 = new LinkedList<>();
        LinkedList<Part> CC1 = new LinkedList<>();
        LinkedList<Part> CC2 = new LinkedList<>();

        for (int i = 0; i<5;i++) {
            AA1.add(new Part("AA1"));
            AA2.add(new Part("AA2"));
            BB1.add(new Part("BB1"));
            BB2.add(new Part("BB2"));
            CC1.add(new Part("CC1"));
            CC2.add(new Part("CC2"));
        }

        storeCar.put("AA1", AA1);
        storeCar.put("AA2", AA2);
        storeCar.put("BB1", BB1);
        storeCar.put("BB2", BB2);
        storeCar.put("CC1", CC1);
        storeCar.put("CC2", CC2);

        Toyota toyota = new Toyota();
        Lada lada = new Lada();
        Kamaz kamaz = new Kamaz();

        try {
            createCar(toyota, storeCar);
            createCar(lada, storeCar);
            createCar(kamaz, storeCar);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }


    public static LinkedList toCheckAnnotation(Field field, HashMap<String, LinkedList<Part>> skladParts) {
        Applicable app = field.getAnnotation(Applicable.class);
        if (app != null) {
            for (String code : app.details()) {
                LinkedList sklad = skladParts.get(code);
                if (skladParts.containsKey(code)) {
                    return sklad;
                }
            }
        }
        return null;

    }


    public static void createCar(Object obj, HashMap<String, LinkedList<Part>> storeCar) throws IllegalAccessException, InstantiationException {

        Class aclass = obj.getClass();
        Object newCar = aclass.newInstance();
        Field[] fields = aclass.getDeclaredFields();
        for (Field f : fields) {
            if (toCheckAnnotation(f,storeCar).size()!=0) {
                f.set(newCar, toCheckAnnotation(f, storeCar).pop());
            } else {
                System.out.println("No details");
            }
            System.out.println(f.get(newCar));
            continue;
        }
    }
}








