package HomeWork.Cars;

/**
 * Created by Ксю on 26.11.2014.
 */
public class Part {

   private String sectorCode;

    public String getSectorCode() {
        return sectorCode;
    }

    public void setSectorCode(String sectorCode) {
        this.sectorCode = sectorCode;
    }

    public Part (){ };

    public Part(String sectorCode){
        this.sectorCode = sectorCode;
    }

}
