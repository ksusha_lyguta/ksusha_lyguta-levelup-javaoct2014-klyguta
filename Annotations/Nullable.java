package HomeWork.Annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Ксю on 23.11.2014.
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)

public @interface Nullable {
    boolean value();

}
