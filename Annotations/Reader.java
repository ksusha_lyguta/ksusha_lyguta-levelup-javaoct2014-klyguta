package HomeWork.Annotations;

import HomeWork.Reflection.Book;

/**
 * Created by Ксю on 23.11.2014.
 */
    public class Reader {

        @Nullable (true)
        private String firstName;
        @Nullable (false)
        private String lastName;
        @Nullable (true)
        private int age;



        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public void createBook(String author, String name) {
            System.out.print("This is good book: " + " " + author + " " + name);
        }

    }


