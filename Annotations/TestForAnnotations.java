package HomeWork.Annotations;

import HomeWork.Reflection.TestReflctionMain;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Created by Ксю on 24.11.2014.
 */
public class TestForAnnotations {

    public static void main(String[] args) {
        TestForAnnotations test = new TestForAnnotations();
        HashMap<String, Object> hash = new HashMap<>();
        hash.put("Name", "SomeName");
        hash.put("Number", 31);
        hash.put("Value", false);
    }

    private void doTest() {
        Class testClass = TestReflctionMain.class;
        Annotation [] annotations = testClass.getAnnotations();

        for (Annotation annotation: annotations){
            if (annotation.annotationType().equals(Runnable.class)){
                Nullable nullable = (Nullable)annotation;
                System.out.println(nullable.value());
            }

        }
    }

    private void doCreateObject (HashMap<String,Object> fieldValues){
        try {
            Class user = Class.forName("HomeWork.Annotations.TestForAnnotations");
            Object object = user.newInstance();
            Field [] fields = user.getDeclaredFields();
            for (Field field :fields){
                Object value  = fieldValues.get(field.getName());
                if (value ==null&&checkNullable(field)){
                    System.out.println("Can't set null into field" + field.getName());
                    throw new RuntimeException("Can't set null into field" + field.getName());
                }
                if (value!=null){
                    setValueIntoField(field.getName(),value,user);
                }
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void setValueIntoField(String name, Object value, Object user) {
        String firstChar = name.charAt(0) + "";
        String methodName = "set" + name.replace(name.charAt(0) + "", firstChar.toUpperCase());
        try {
            Method method = user.getClass().getMethod(methodName, value.getClass());
            method.invoke(user, value);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    public boolean checkNullable(Field field) {
        Nullable nullable = field.getAnnotation(Nullable.class);
        if (nullable != null) {
            if (nullable.value()) {
                return true;
            }
        }
        return false;
    }
}
