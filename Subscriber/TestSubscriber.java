package HomeWork.Subscriber;

import HomeWork.Cars.Toyota;

/**
 * Created by Ксю on 13.12.2014.
 */
public class TestSubscriber {
    static String message = "Test";
    private static Object lock = new Object();

    public static void main(String[] args) {
        Thread topic = new Thread(new Topic(message, lock));
        Thread subscriber = new Thread(new Subscriber(message, lock));

        topic.start();
        subscriber.start();

    }
}
