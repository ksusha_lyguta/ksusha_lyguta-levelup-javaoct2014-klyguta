package HomeWork.Subscriber;

/**
 * Created by Ксю on 13.12.2014.
 */
public class Topic implements Runnable {

    public static String message;
    public Object lock;

    public Topic(String message, Object lock) {
        this.message = message;
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock) {
                try {
                    Thread.sleep(5000);
                    System.out.println("Sensation");
                    lock.notifyAll();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }





