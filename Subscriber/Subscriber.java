package HomeWork.Subscriber;


/**
 * Created by Ксю on 13.12.2014.
 */
public class Subscriber implements Runnable {
    public static String message = Topic.message;
    Object lock = new Object();

    public Subscriber(String message, Object lock) {
        this.message = message;
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock) {

            while (true) {
                System.out.println("We have got a new message " + message);
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
