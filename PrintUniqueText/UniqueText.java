package HomeWork.PrintUniqueText;

import java.io.File;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Created by Ксю on 19.11.2014.
 * Есть структура каталогов, в каждой папке есть файлы с расширением txt.
 * Вывести список уникальных, неповторяющихся файлов, которые содержат txt.
 */
public class UniqueText {

    public static void main(String[] args) {

        uniqueText("C:/Users/Ксю/Desktop/Folder");

    }

    public static void uniqueText(String src) {
        File list = new File(src);
        TreeSet<String> text = new TreeSet<>(new UniqueNameComparator());
        if (list.isDirectory()) {
            File[] file = list.listFiles();
            for (File f1 : file) {
                if (list.isFile()) {
                    text.add(f1.getName());
                } else uniqueText(f1.getAbsolutePath());
            }
            for (String s : text){
                System.out.println(s);
            }
        }
    }

    public static class UniqueNameComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            return o1.length() - o2.length();
        }
    }

}


