package HomeWork.DBEngine;

import java.lang.annotation.*;


/**
 * Created by Ксю on 09.12.2014.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Column {
    String name();

}


