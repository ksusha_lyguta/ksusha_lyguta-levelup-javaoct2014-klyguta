package HomeWork.DBEngine;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Ксю on 09.12.2014.
 */
public class DBEngine {


    private DBManager dbManager = DBManager.getInstance();
    private Connection connection;
    private PreparedStatement userInsertQuery;
    private PreparedStatement userCreateQuery;
    private Class claz;

    public DBEngine(Class claz) {
        this.claz = claz;
        try {
            connection = dbManager.getConnection();
            userCreateQuery =
                    connection.prepareStatement(createSelectQuery(claz),
                            Statement.RETURN_GENERATED_KEYS);
            userInsertQuery =
                    connection.prepareStatement(createInsertQuery(claz),
                            Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static String returnTableName(Class claz) throws
            ClassNotFoundException, IllegalAccessException, InstantiationException {
        Object obj = Class.forName(claz.getName()).newInstance();
        Table table = obj.getClass().getAnnotation(Table.class);
        if (table.name() != null) {
            return table.name();
        }
        return null;
    }

    public static String returnColumnName(Class claz) throws
            IllegalAccessException, InstantiationException,
            ClassNotFoundException {

        ArrayList<String> list = new ArrayList<String>();
        Object obj = claz.newInstance();
        Field[] field = claz.getDeclaredFields();
        for (Field fields : field) {
            list.add(checkAnnoColumn(fields));
        }
        return String.valueOf(list);
    }

    public static String checkAnnoColumn(Field field) {
        Column column = field.getAnnotation(Column.class);
        if (column != null && !column.name().equals("id")) {
            return column.name() + " ,";
        }
        if (column != null && column.name().equals("id")) {
            return "";
        }
        return null;
    }

    public String createSelectQuery(Class claz) throws
            IllegalAccessException, InstantiationException, ClassNotFoundException {
        return "Select id, " + returnColumnName(claz) + "from " +
                "levelup. " + returnTableName(claz) + "where id=?";
    }

    public String createInsertQuery(Class claz) throws
            IllegalAccessException, InstantiationException, ClassNotFoundException {
        return "Insert into levelup." + returnTableName(claz) + " (" +
                returnColumnName(claz) + ")" + " values ";
    }

    public void close() {

        try {
            userCreateQuery.close();
            userInsertQuery.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}