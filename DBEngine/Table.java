package HomeWork.DBEngine;
import java.lang.annotation.*;
import java.util.Date;


/**
 * Created by Ксю on 09.12.2014.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)

public @interface Table {
    String name();
}
