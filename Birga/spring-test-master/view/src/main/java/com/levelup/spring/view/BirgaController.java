package com.levelup.spring.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Random;

/**
 * Created by Ksu on 23.03.2015.
 */

@Controller
public class BirgaController {

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public
    String index(Model model) {

        Random rand = new Random();
        float r = rand.nextFloat() * 1000;
        model.addAttribute("price",r);
        return "index";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public
    String randomForSale(Model model) {

        Random rand = new Random();
        float r = rand.nextFloat() * 1000;
        model.addAttribute("price",r);
        return "page";
    }
}



