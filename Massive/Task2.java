package HomeWork.Massive;

/**
 * Created by Ксю on 15.10.2014.
 */
public class Task2 {
    public static void main(String[] args) {
        int[] massive1 = {1, 2, 2};
        int[] massive2 = {4, 4, 1, 2, 2};
        System.out.println(either(massive1));
        System.out.println(either(massive2));
    }

    static boolean either(int arr[]) {
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == 2 && arr[i + 1] == 2 || arr[i] == 4 && arr[i + 1] == 4) {
                return true;
            }
        }
        return false;
    }
}
