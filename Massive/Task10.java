package HomeWork.Massive;

/**
 * Created by Ксю on 17.10.2014.
 */
public class Task10 {
    public static void main(String[] args) {
        int mass1[] = {1, 4, 5, 6, 2};
        int mass2[] = {1, 2, 4};
        System.out.println(tripleUp(mass1));
        System.out.println(tripleUp(mass2));
    }

    static boolean tripleUp(int[] arr) {
        for (int i = 0; i < arr.length-2; i++) {
            if (arr[i]+1==arr[i+1]&&arr[i+1]+1==arr[i+2]) {
                return true;
            }
        }
        return false;
    }
}
