package HomeWork.Massive;

/**
 * Created by Ксю on 16.10.2014.
 */
public class Task4 {
    public static void main(String[] args) {
        int[] massive1 = {4, 7, 7, 3};
        int[] massive2 = {7, 7, 4, 7};
        System.out.println(twoTwo(massive1));
        System.out.println(twoTwo(massive2));
    }

    static boolean twoTwo(int arr[]) {
        int count=0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == 7 && arr[i + 1] == 7) {
                count++;
            } else if (arr[i] == 7 && arr[i + 1] != 7) {
                count--;
            }
            if (count>0){
                return true;
            }
        }
        return false;
    }
}
