package HomeWork.Massive;

/**
 * Created by Ксю on 16.10.2014.
 */
public class Task7 {
    public static void main(String[] args) {
        int[] mass1 = {1, 0, 0, 1};
        int[] mass2 = {1, 0};
        zeroFront(mass1);
        for (int x = 0; x < mass1.length; x++) {
            System.out.print(mass1[x]);
        }
        System.out.println();
        zeroFront(mass2);
        for (int x = 0; x < mass2.length; x++) {
            System.out.print(mass2[x]);
        }
    }

    static int[] zeroFront(int[] mass) {
        int z = 0;
        for (int i = 0; i < mass.length; i++) {
            if (mass[i] == 0) {
                mass[i] = mass[z];
                mass[z] = 0;
                z++;
            }
        }
        return mass;
    }
}


