package HomeWork.Massive;

/**
 * Created by Ксю on 15.10.2014.
 */
public class Task3 {
    public static void main(String[] args) {
        int massive1[] = {1, 3, 2};
        int massive2[] = {3, 1, 4, 5, 6};
        System.out.println(has12(massive1));
        System.out.println(has12(massive2));
    }

    static boolean has12(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            while (arr[i] == 2) {
                return true;
            }
        }
        return false;
    }
}
