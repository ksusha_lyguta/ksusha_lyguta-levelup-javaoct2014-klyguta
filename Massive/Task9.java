package HomeWork.Massive;

/**
 * Created by Ксю on 16.10.2014.
 */
public class Task9 {
    public static void main(String[] args) {
        int[] mass1 = {1, 2, 2};
        int[] mass2 = {1, 2, 2, 6, 99, 99, 7};
        int[] mass3 = {1, 1, 6, 7, 2};

        sum67(mass1);
        System.out.println();
        sum67(mass2);
        System.out.println();
        sum67(mass3);

    }

    static void sum67(int[] mass) {
        int count = 0;
        boolean flag = true;
        for (int i =0; i < mass.length; i++){
            if (mass [i]==6){
                flag = false;
                break;
            }
            if (mass [i] ==7){
                flag =true;
                continue;
            }
            if (flag){
                count += mass [i];
            }
        }
        System.out.print(count);
    }
}
