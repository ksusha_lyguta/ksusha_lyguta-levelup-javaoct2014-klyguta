package HomeWork.Massive;

/**
 * Created by Ксю on 16.10.2014.
 */
public class Task8 {
    public static void main(String[] args) {
        int[] mass1 = {1, 0, 1, 0, 0, 1, 1};
        int[] mass2 = {3, 3, 2};
        evenOdd(mass1);
        for (int y = 0; y < mass1.length; y++) {
            System.out.print(mass1[y]);
        }
        System.out.println();
        evenOdd(mass2);
        for (int y = 0; y < mass2.length; y++) {
            System.out.print(mass2[y]);
        }
    }

    static int[] evenOdd(int[] arr) {
        int x = 0;
        int y = arr.length - 1;
        int[] array = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                array[x] = arr[i];
                x++;
            } else {
                array[y] = arr[i];
                y--;
            }
        }
        return arr;
    }
}
