package HomeWork.Reflection;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.lang.*;

/**
 * Created by Ксю on 24.11.2014.
 */
public class Test {
    public static void main(String[] args) throws Exception {


        HashMap<String, Object> map = new HashMap();
        map.put("Vasya", new Reader());
        map.put("Petya", new Reader());
        map.put("Vova", new Reader());
        System.out.println(map);

        Class c1 = Class.forName(map.get("Vasya").getClass().getName());
        Object obj1 = c1.newInstance();
        Field fieldVasya1 = obj1.getClass().getField("firstName");
        fieldVasya1.set(obj1, "Vasya");
        System.out.println(fieldVasya1.get(obj1));
        Field fieldVasya2 = obj1.getClass().getField("lastName");
        fieldVasya2.set(obj1, "Petrov");
        System.out.println(fieldVasya2.get(obj1));
        Field fieldVasya3 = obj1.getClass().getField("age");
        fieldVasya3.set(obj1, 23);
        System.out.println(fieldVasya3.get(obj1));
        Field fieldVasya4 = obj1.getClass().getField("Book");
        fieldVasya4.set(obj1, new Book());
        System.out.println(fieldVasya4.get(obj1));
    }
}
