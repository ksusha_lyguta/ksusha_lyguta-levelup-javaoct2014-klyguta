package HomeWork.Reflection;


/**
 * Created by Ксю on 21.11.2014.
 */
public class Reader {
    private String firstName;
    private String lastName;
    private int age;
    Book book;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void createBook(String author, String name) {
       System.out.print("This is good book: " + " " + author + " " + name);
    }

}
