package HomeWork.Reflection;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * Created by Ксю on 21.11.2014.
 *
 */
public class TestReflection {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        Book book = new Book();
        Reader reader = new Reader();
        // объекты класса Book
        Class classBook = book.getClass();
        String s = classBook.getName();
        System.out.println(classBook);
        System.out.println(s);
        Field field = classBook.getDeclaredField(("name"));
        System.out.println("Declared Field of Book is " + classBook.getDeclaredField("author"));

        Constructor [] constructors = classBook.getDeclaredConstructors();
        for (Constructor constructor: constructors){
            Class [] paramtypes = constructor.getParameterTypes();
            for (Class paramType: paramtypes){
                System.out.println(paramType.getName()+ " ");
            }
            System.out.println();
        }


        String nameBook = classBook.getName();
        System.out.println(nameBook);
        String classNameBook = "HomeWork.Reflection.Book";
        Class nameBook2 = Class.forName(classNameBook);
        System.out.println(nameBook2);
        Object m1 = book.getClass().newInstance();
        Object m2 = Class.forName(nameBook).newInstance();

        String modifiers = Modifier.toString(classBook.getModifiers());
        System.out.println(modifiers);
        System.out.println(classBook.getDeclaredConstructors());
        System.out.println(classBook.getDeclaredFields());
        System.out.println(classBook.getDeclaredMethods());
        Method[] methods = classBook.getMethods();
        for (Method m : methods) {
            System.out.println(m);
        }
        System.out.println();
        Field[] fileds = classBook.getDeclaredFields();
        for (Field f: fileds){
            System.out.println(f);
        }

        // объекты класса Reader
        Class classReader = reader.getClass();
        String nameReader = classReader.getName();
        System.out.println(nameReader);
        String classNameReader = "HomeWork.Reflection.Reader";
        Class nameReader2 = Class.forName(classNameReader);
        System.out.println(classNameReader);


    }
}
