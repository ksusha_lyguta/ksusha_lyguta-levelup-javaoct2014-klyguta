package HomeWork.Reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ксю on 22.11.2014.
 * Есть два класса, Reader с полями String firstName, String lastName, int age, Book book и класс
 * Book с полями String author, String name. Оба класса должны иметь setters/getters, конструкторы по умолчанию.
 * Необходимо: с помощью reflection api создать экземпляры класса Reader и Book (читай про Class.forName()), а также все с помощью той же рефлексии
 * просетить поля объектов вышеуказанных классов. Сетить можно напрямую в поля, либо вызывая методы сет/гет (вызывать через рефлексию)
 * Задание :в классе Reader добавить метод c сигнатурой createBook(String author, String name). Через
 * рефлексию вызвать этот метод
 * дан HashMap<String, Object>, String - описывает имена полей класса, Object
 * - значения полей. Получить пару имя поля/значие и через рефлексию засетить/установить в объект (объект создать, наверное тоже через Class.forName)
 */
public class TestReflctionMain {
    public static void main(String[] args) throws Exception {
//        Book b = new Book();
//        Class b1 = b.getClass();
//        Book book = (Book)b1.newInstance();

//        Class aclass = Book.class;
//        Book book = (Book) aclass.newInstance();
        Class class1 = Class.forName("HomeWork.Reflection.Book");
        Book book = (Book) class1.newInstance();

        Field forBookAuthor = book.getClass().getDeclaredField("author");
        forBookAuthor.setAccessible(true);
        forBookAuthor.set(book, "Pushkin");
        System.out.println(forBookAuthor.get(book));

        Field forBookName = book.getClass().getDeclaredField("name");
        forBookName.setAccessible(true);
        forBookName.set(book, "Poems");
        System.out.println(forBookName.get(book));

        Class class2 = Class.forName("HomeWork.Reflection.Reader");
        Reader reader = (Reader) class2.newInstance();

        Field forAuthorFirstname = reader.getClass().getDeclaredField("firstName");
        forAuthorFirstname.setAccessible(true);
        forAuthorFirstname.set(reader, "Taras");
        System.out.println(forAuthorFirstname.get(reader));

        Field forAuthorLastName = reader.getClass().getDeclaredField("lastName");
        forAuthorLastName.setAccessible(true);
        forAuthorLastName.set(reader, "Shevchenko");
        System.out.println(forAuthorLastName.get(reader));

        Field forAge = reader.getClass().getDeclaredField("age");
        forAge.setAccessible(true);
        forAge.set(reader, 45);
        System.out.println(forAge.get(reader));

        Field forBook = reader.getClass().getDeclaredField("book");
        forBook.setAccessible(true);
        forBook.set(reader, new Book());

        Class[] paramtypes = new Class[]{String.class, String.class};
        Method m = reader.getClass().getDeclaredMethod("createBook", paramtypes);
        Object[] arg = new Object[]{new String("No author"), new String("Enciclopedia")};
        m.invoke(reader, arg);

        System.out.println();

        HashMap<String, Object> hash = new HashMap<>();
        hash.put("Detective", new Book());
        hash.put("Poems", new Book());
        hash.put("Fantastic", new Book());


        Class c1 = Class.forName(hash.get("Detective").getClass().getName());
        Object detective = c1.newInstance();
        Field forDetective1 = detective.getClass().getDeclaredField("author");
        forDetective1.setAccessible(true);
        forDetective1.set(detective, "Kovalenko");
        System.out.println(forDetective1.get(detective));
        Field forDetective2 = detective.getClass().getDeclaredField("name");
        forDetective2.setAccessible(true);
        forDetective2.set(detective, "Who is murderer");
        System.out.println(forDetective2.get(detective));

        Class c2 = Class.forName(hash.get("Poems").getClass().getName());
        Object poems = c2.newInstance();
        Field forPoems = poems.getClass().getDeclaredField("author");
        forPoems.setAccessible(true);
        forPoems.set(poems, "Ahmatova");
        System.out.println(forPoems.get(poems));
        Field forPoems2 = poems.getClass().getDeclaredField("name");
        forPoems2.setAccessible(true);
        forPoems2.set(poems, "Sbornik");
        System.out.println(forPoems2.get(poems));

        Class c3 = Class.forName(hash.get("Fantastic").getClass().getName());
        Object fantastic = c3.newInstance();
        Field forFantastic = fantastic.getClass().getDeclaredField("author");
        forFantastic.setAccessible(true);
        forFantastic.set(fantastic, "Rowling");
        System.out.println(forFantastic.get(fantastic));
        Field forFantastic2 = fantastic.getClass().getDeclaredField("name");
        forFantastic2.setAccessible(true);
        forFantastic2.set(fantastic, "Harry Potter");
        System.out.println(forFantastic2.get(fantastic));
        System.out.println(hash);

        hash.put("Fantastic",detective);
        hash.put ("Poems", poems);
        hash.put("Fantestic", fantastic);
        System.out.println(hash);


        }

    }
