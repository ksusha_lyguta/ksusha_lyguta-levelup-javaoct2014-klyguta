package HomeWork.Serialization.SerializationFromKrava;

/**
 * Created by Ксю on 18.12.2014.
 */
public class User {
    private String firstName;
    private String lastName;

    public User() {
    }

    public User(String firstSymbol, String lastSymbol) {
        this.firstName = firstSymbol;
        this.lastName = lastSymbol;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

