package HomeWork.Serialization.SerializationFromKrava;

import java.io.IOException;

/**
 * Created by Ксю on 18.12.2014.
 */
public class Main {
    public static void main(String[] args)
            throws IllegalAccessException, NoSuchFieldException, IOException, InstantiationException, ClassNotFoundException {

        User user = new User("Aleksey", "Kravchenko");
        Serializator.serializeToTxt(user);
        String fileName = "D:/Java/LevelUp/src/HomeWork/SomeTest.txt";
        User restoredUser = (User) Serializator.deserializeFromTxt(fileName);
        System.out.println(restoredUser.toString());
    }
}