package HomeWork.Serialization.SerializationFromKrava;

import java.io.*;
import java.lang.reflect.Field;

/**
 * Created by Ксю on 18.12.2014.
 */
public class Serializator {
    public static void serializeToTxt(Object obj) throws IllegalAccessException, NoSuchFieldException, IOException {
        String data;
        String separator = ":";
        data = "class" + separator + obj.getClass().getCanonicalName() + "\n";
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            data += "field" + separator + field.getName()
                    + separator + field.get(obj) + "\n";
        }
        FileWriter out = new FileWriter(new File("D:/Java/LevelUp/src/HomeWork/SomeTest.txt"));
        out.write(data + "\n");
        out.flush();
        out.close();
    }

    public static Object deserializeFromTxt(String fileName)
            throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        String className;
        String fieldName;
        String fieldValue;
        Class aClass;
        Object obj = null;
        String s;
        while ((s = in.readLine()) != null) {
            if (s.trim().startsWith("class")) {
                className = s.substring(s.indexOf(":") + 1, s.length());
                aClass = Class.forName(className);
                obj = aClass.newInstance();
            }
            if (s.trim().startsWith("field")) {
                fieldName = s.substring(s.indexOf(":") + 1, s.lastIndexOf(":"));
                Field field = obj.getClass().getDeclaredField(fieldName);
                fieldValue = s.substring(s.lastIndexOf(":") + 1, s.length());
                field.setAccessible(true);
                field.set(obj, fieldValue);
            }
        }
        in.close();
        return obj;
    }
}