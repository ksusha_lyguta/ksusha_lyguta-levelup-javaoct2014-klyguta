package HomeWork.Serialization;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Created by Ксю on 17.12.2014.
 */


        public class SerializingFromFile {

            public static void readObject(Object o) throws ClassNotFoundException,
                    IllegalAccessException, InstantiationException, IOException {

                Class clazz = Class.forName("HomeWork.Serialization.MySerialization.SerializingFromFile");
                String text = "class name " + clazz.getSimpleName();
                Object obj = clazz.newInstance();
                Field[] publicFields = clazz.getDeclaredFields();
                for (Field field : publicFields) {

                    text += "\r\n" + field.getType().getSimpleName() + " " + field.getName() + "=" + field.get(obj) + " ";

                }
                System.out.println(text);
                FileOutputStream fos = new FileOutputStream("test.txt");
                ObjectOutputStream oos = new ObjectOutputStream(fos);

                oos.writeObject(text);
                oos.flush();
                fos.flush();
                oos.close();
                fos.close();
            }

        }

        class SerializingTest {
            public static void main(String[] args) {
                SerializingFromFile serFile = new SerializingFromFile();
                try {
                    serFile.readObject("HomeWork.Serialization.MySerialization.SerializingFromFile");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

