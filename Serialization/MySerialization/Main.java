package HomeWork.Serialization.MySerialization;

import java.io.IOException;

/**
 * Created by Ксю on 19.12.2014.
 */
public class Main {
    public static void main(String[] args) throws IOException, IllegalAccessException, NoSuchFieldException, InstantiationException, ClassNotFoundException {
        SomeTest test = new SomeTest("31", "Ksusha");
        TestSerialization testSerialization = new TestSerialization();
        testSerialization.serializeTo(test);
        SomeTest test2 = (SomeTest) TestSerialization.serializeFrom("D:/Java/LevelUp/src/HomeWork/SomeTest.txt");
        System.out.println(test2);
    }
}
