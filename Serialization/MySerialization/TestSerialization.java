package HomeWork.Serialization.MySerialization;

import java.io.*;
import java.lang.reflect.Field;

/**
 * Created by Ксю on 18.12.2014.
 */
public class TestSerialization {

    public static void serializeTo(Object object) throws IllegalAccessException, NoSuchFieldException, IOException {
        String s = object.getClass().getName() + "\n";
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field f : fields) {
            f.setAccessible(true);
            s += f.getName() + " " + f.get(object) + "\n";
        }

        FileWriter file = new FileWriter(new File("D:/Java/LevelUp/src/HomeWork/SomeTest.txt"));
        try {
            file.write(s);
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object serializeFrom(String s)
            throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        BufferedReader reader = new BufferedReader(new FileReader(s));
        String className = null;
        String fieldName  = null;
        String fieldValue = null;
        Class clazz;
        Object obj = null;
        String test;

        boolean classIniialized = false;

        while ((test = reader.readLine()) != null)
            if (!classIniialized)
            {
                clazz = Class.forName(test);
                obj = clazz.newInstance();
                classIniialized = true;
            }
            else
            {
                Field field = obj.getClass().getDeclaredField(test.split(" ")[0]);
                field.setAccessible(true);

                field.set(obj, test.split(" ")[1]);


            }

        reader.close();
        return obj;
    }
}


