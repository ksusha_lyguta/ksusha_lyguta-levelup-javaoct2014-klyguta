package HomeWork.Serialization.MySerialization;


import java.io.Serializable;

/**
 * Created by Ксю on 17.12.2014.
 */

public class SomeTest implements Serializable {

    private String number;
    private String name;

    public SomeTest()
    {}

    public SomeTest(String number, String name) {
        this.number = number;
        this.name = name;

    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}



