package httpDownloader;

import stock.StockInTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Ksu on 21.06.2015.
 */
public class HistoryHttpDownloader extends HttpDownloader {
    public static ArrayList<StockInTime> getHistoricalQuotes(String symbol, Date from, Date to) {
        String data = downloadFile(getHistoryURI(symbol, from, to));
        ArrayList<StockInTime> stockHistory = parseHistoryData(data);
        return stockHistory;
    }

    private static String getHistoryURI(String symbol, Date from, Date to) {
        Calendar fromDate = new GregorianCalendar();
        fromDate.setTime(from);
        Calendar toDate = new GregorianCalendar();
        toDate.setTime(to);
        String uri = "http://ichart.finance.yahoo.com/table.csv?s=";
        uri += symbol;
        uri += "&a=" + fromDate.get(Calendar.MONTH);
        uri += "&b=" + fromDate.get(Calendar.DAY_OF_MONTH);
        uri += "&c=" + fromDate.get(Calendar.YEAR);
        uri += "&d=" + toDate.get(Calendar.MONTH);
        uri += "&e=" + toDate.get(Calendar.DAY_OF_MONTH);
        uri += "&f=" + toDate.get(Calendar.YEAR);
        return uri += "g=d";

    }

    public static ArrayList<StockInTime> parseHistoryData(String data) {
        ArrayList<StockInTime> stockHistory = new ArrayList<StockInTime>();
        String[] csvRows = data.split("\n");

        for (int i = 0; i < csvRows.length; i++) {
            String[] stockInfo = csvRows[i].split(",");
            StockInTime stockPoint = new StockInTime(convertToDate(stockInfo[0]));
            parseDouble(stockInfo[4]);
            stockHistory.add(stockPoint);
        }
        return stockHistory;
    }

    private static Date convertToDate(String sDate) {
        try {
            DateFormat dateformater = new SimpleDateFormat("yyyy-MM-dd");
            return dateformater.parse(sDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

}
