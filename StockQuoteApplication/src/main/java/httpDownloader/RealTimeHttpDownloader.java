package httpDownloader;

import stock.Stock;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Ksu on 20.06.2015.
 */
public class RealTimeHttpDownloader extends HttpDownloader {
    private static ArrayList<String> currentSymbols = new ArrayList<String>();

    public static boolean currentSymbolsIsEmpty() {
        return currentSymbols.isEmpty();

    }

    public static String getRealTimeQuotes() {
        return downloadFile(getRealTimeURI());
    }

    public static String addStockQuote(String symbol) {
        if (!currentSymbols.contains(symbol)) {
            currentSymbols.add(symbol);
        }
        System.out.println(currentSymbols.toString());
        return symbol;
    }

    public static void removeStockQuote(String symbol) {
        currentSymbols.remove(symbol);
    }
    private static String getRealTimeURI(){
        String uri = "http://finance.yahoo.com/d/quotes.csv?s=";
        for (int i = 0; i<currentSymbols.size(); i++){
            uri += currentSymbols.get(i);
            if (i+1 < currentSymbols.size()){
                uri += "+";
            }
        }
        return uri += "&f=snllc6ohgd1t1";
    }
    public static ArrayList<Stock> parseRealTimeData (String data){
        ArrayList<Stock> newStocks = new ArrayList<Stock>();
        String [] csvRows = data.split("\n");
        for (int i =0; i<csvRows.length; i++){
            String [] stockInfo = csvRows[i].split(",");
            Stock aStock = new Stock (stockInfo[0].replace("\"",""));
            stockInfo[1].replace("\"","");
            parseDouble(stockInfo[2]),
            parseDouble(stockInfo[3]),
            parseDouble(stockInfo[4]),
            parseDouble(stockInfo[5]),
            parseDouble(stockInfo[6]),
                    convertToDate(stockInfo[7], stockInfo[8]));
            newStocks.add(aStock);
        }
        return newStocks;
    }
    private static Date convertToDate (String sDate, String sTime){
        sDate = sDate.replace("\"","");
        sDate = sDate.replace("\r","");
        sTime = sTime.replace("\"","");
        sTime = sTime.replace("\r","");
        sDate += " " + sTime;

    try {
        DateFormat dateformater = new SimpleDateFormat("M/d/yyyy KK:mmaa");
        return dateformater.parse(sDate);
    } catch (ParseException e){
        e.printStackTrace();
    }
        return null;
}
}
