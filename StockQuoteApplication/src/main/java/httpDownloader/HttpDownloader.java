package httpDownloader;

import sun.net.www.http.HttpClient;

import javax.xml.ws.http.HTTPException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Ksu on 12.06.2015.
 */
public class HttpDownloader {

    protected static String downloadFile(String uri) {
        HttpClient httpClient = new HttpClient();
        HttpMethod getMethod = newGetMethod(uri);

        try {
            int response = httpClient.executeMethod(getMethod);
            if (response != 200) {
                throw new HttpException("HTTP problem, httpcode: " + response);
            }

            InputStream inputStream = getMethod.getResponseBodyAsStream();
            String responseText = streamToString(inputStream);
            return responseText;
        } catch (HTTPException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    private static String streamToString (InputStream inputStream) throws IOException{
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        StringBuilder stringBuilder = new StringBuilder();
        byte [] buffer = new byte [1024];
        int bytesRead = 0;
        while ((bytesRead = bufferedInputStream.read(buffer))!=-1){
            stringBuilder.append(new String (buffer,0,bytesRead));
        }
        return stringBuilder.toString();
    }
    protected static Double parseDouble (String number){
        number = number.replace("\"","");
        try {
            return Double.parseDouble(number);
        } catch (NumberFormatException e){
            return 0.0;
        }
    }

}
