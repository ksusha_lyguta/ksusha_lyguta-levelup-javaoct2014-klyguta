package stock;

import java.util.Date;

/**
 * Created by Ksu on 22.06.2015.
 */
public class StockInTime {
    public Date date;
    public Double close;

    public StockInTime(Date date, Double close) {
        this.date = date;
        this.close = close;
    }

    public StockInTime(Date date) {

    }
}
