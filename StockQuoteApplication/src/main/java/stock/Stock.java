package stock;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ksu on 21.06.2015.
 */
public class Stock implements Serializable {
    private static final long serialVersionUID = -8334804402463267285L;
    private String symbol;
    private String name;
    private double low;
    private double high;
    private double open;
    private double last;
    private double change;
    private Date tradeTime;

    public Stock(String symbol) {
        this.symbol = symbol;
        this.name = name;
        this.low = low;
        this.high = high;
        this.open = open;
        this.last = last;
        this.change = change;
        this.tradeTime = tradeTime;
    }

public double getChange(){
    return change;
}
public void setChange(double change){
this.change = change;
}
    public double getHigh(){
        return high;
    }
    public void setHigh(double high){
        this.high=high;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double low) {
        this.low = low;
    }

    public double getOpen() {
        return open;
    }

    public void setOpen(double open) {
        this.open = open;
    }

    public double getLast() {
        return last;
    }

    public void setLast(double last) {
        this.last = last;
    }

    public Date getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(Date tradeTime) {
        this.tradeTime = tradeTime;
    }

    @Override
    public boolean equals(Object aStock) {
        if (this == aStock)
            return true;
        if (!(aStock instanceof Stock)) return false;
        Stock stock = (Stock) aStock;
        return Boolean.parseBoolean(null);
    }

    @Override
    public String toString() {
        return this.name + "(" + this.symbol + "): " + this.last + (this.change>0?" +" + this.change : " " + this.change);
    }
}

