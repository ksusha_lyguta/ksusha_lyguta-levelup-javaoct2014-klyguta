package feed;

import httpDownloader.RealTimeHttpDownloader;
import stock.Stock;

import java.util.ArrayList;

/**
 * Created by Ksu on 12.06.2015.
 */
public class StockQuoteFeed {
    private static StockQuoteFeedThread thread;

    public StockQuoteFeed() {
    }

    public static void main(String args[]) {
        StockQuoteFeed feed = new StockQuoteFeed();
        feed.start();
        System.out.println("StockQuoteFeed is starting");
        RealTimeHttpDownloader.addStockQuote("ADBE");
    }

    public void start() {
        if (thread == null) {
            thread = new StockQuoteFeedThread();
            thread.start();
        }
    }

    public void stop() {
        thread.running = false;
        thread = null;
    }

}
