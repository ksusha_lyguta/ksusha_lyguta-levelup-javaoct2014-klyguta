package feed;

import httpDownloader.RealTimeHttpDownloader;
import stock.Stock;

import java.util.ArrayList;

/**
 * Created by Ksu on 22.06.2015.
 */
public class StockQuoteFeedThread extends Thread {
    public static int test = 0;
    public boolean running = true;

    public static ArrayList<Stock> compareStocks (ArrayList<Stock> currentStocks, ArrayList<Stock> newStocks){
        ArrayList<Stock> changes = new ArrayList<Stock>();
        changes = (ArrayList<Stock>) newStocks.clone();
        changes.removeAll(currentStocks);
        return changes;

    }
    public void run(){
        ArrayList<Stock> currentStocks = new ArrayList<Stock>();
        MessageBroker msgBroker = MesaageBrocker.getMessagerBrocker(null);
        String clientId = UUIDUtils.createUUID();
        while(running){
            if(!RealTimeHttpDownloader.currentSymbolsIsEmpty()){
                String data = RealTimeHttpDownloader.getRealTimeQuotes();
                ArrayList<Stock> changes = compareStocks(currentStocks,newStocks);
                currentStocks = (ArrayList<Stock>)newStocks.clone();
                for (int i=0; i<changes.size();i++){
                    AsyncMessage msg = new AsyncMessage();
                    msg.setDestination("stockFeed");
                    msg.setHeader("DSSubtopic", changes.get(i).getSymbol());
                    msg.setClientId(clientId);
                    msg.setMessageId(UUIDUtils.createUUID());
                    msg.setTimestamp(System.currentTimeMillis());
                    msg.setBody(changes.get(i));
                    msgBroker.routeMessageToService(msg,null);
                }
            }
        }
    }
}
}


        }
