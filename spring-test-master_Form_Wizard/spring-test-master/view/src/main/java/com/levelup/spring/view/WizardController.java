package com.levelup.spring.view;

import com.levelup.spring.model.User;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;


/**
 * Created by Ksu on 23.04.2015.
 */
@Controller
@SessionAttributes({"name", "code", "user"})
@RequestMapping("/wizard")
public class WizardController {


    @RequestMapping("/first")
    public String getFirstForm(Model model) {
        return "main.page";
    }

    @RequestMapping("/next")
    public String getNextForm(Model model, @ModelAttribute("name") User user) {
        model.addAttribute("firstName", user);
        model.addAttribute("lastName", user);
        return "secondStep.jsp";
    }

    @RequestMapping("/last")
    public String nextForm(Model model, @ModelAttribute("code") User user) {
        model.addAttribute("password", user);
        model.addAttribute("code", user);
        return "lastStep.jsp";
    }

    @RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public
    @ResponseBody
    User getUser(@ModelAttribute("user") User user) {

        return user;
    }
}
