<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Ksu
  Date: 23.04.2015
  Time: 0:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>First wizard</title>
    <script src="http://code.jquery.com/jquery-1.11.1.js"></script>
    <script src="/js/wizard.js"></script>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>


<body>
<h2>First form</h2>
<form method="POST" id = "firstStep.jsp" name = "firstStep" action = "firstStep">

    <table>
        <tr>
            <td>First Name :</td>
            <td>
                <input type="text" name="firstName"/>
            </td>
        </tr>
        <tr>
            <td>Last Name :</td>
            <td>
                <input type="text" name="lastName"/>
            </td>
        </tr>
        <tr>
            <td colspan="3"><input type="button" value="next"></td>
        </tr>
    </table>

</form>

</body>
</html>
