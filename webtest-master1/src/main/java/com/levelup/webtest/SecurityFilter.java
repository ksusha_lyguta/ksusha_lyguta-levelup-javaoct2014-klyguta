package com.levelup.webtest;

import com.levelup.webtest.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by denis_zavadsky on 1/31/15.
 */
public class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        StringBuffer url = request.getRequestURL();
        if (!url.toString().endsWith("newlogin.jsp") && !url.toString().endsWith("login")) {


            User userRole = (User) request.getSession().getAttribute("role");

            if (userRole != null) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                ((HttpServletResponse) servletResponse).sendRedirect("content_role.jsp");

            }

        }

    }

    @Override
    public void destroy() {

    }

}
