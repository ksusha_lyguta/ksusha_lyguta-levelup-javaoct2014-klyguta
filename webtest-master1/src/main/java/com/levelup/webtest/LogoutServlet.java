package com.levelup.webtest;

import com.levelup.webtest.dao.DatabaseUtilBean;
import com.levelup.webtest.model.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Ksu on 03.02.2015.
 */
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out=response.getWriter();

        request.getRequestDispatcher("logout.jsp").include(request, response);

        HttpSession session=request.getSession();
        session.invalidate();

        out.print("You are logged out!");

        out.close();
    }
}


