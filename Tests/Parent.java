package HomeWork.Tests;

/**
 * Created by Ксю on 23.10.2014.
 */
public class Parent {
    Parent() {
        System.out.println("Parent()")
        ;
    }

    Parent(int i) {
        System.out.println("Parent(int");
    }
}

class Child extends Parent {
    Child() {
        System.out.println("Child()");
    }

    Child(int i) {
        System.out.println("Child(int i");
    }


    public static void main(String[] args) {
        new Child(0);
    }
}