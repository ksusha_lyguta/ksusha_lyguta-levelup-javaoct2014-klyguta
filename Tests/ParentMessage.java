package HomeWork.Tests;

/**
 * Created by Ксю on 23.10.2014.
 */
public class ParentMessage {
    void call() {
        System.out.println("Parent");
    }
}

class ChildMessage extends ParentMessage {
    public static void main(String[] args) {
        send(new ChildMessage());
    }

    public static void send(ParentMessage msg) {
        msg.call();
    }

    void call() {
        System.out.println("Child");
    }
}
