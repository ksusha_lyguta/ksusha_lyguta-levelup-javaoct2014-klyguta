package com.levelup.spring.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by java on 19.03.15.
 */

@Controller
@RequestMapping("/index")
public class PageController {



    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public int getNumber(int number) {

        int num=(int)(Math.random()*100+1);
        return num;
    }
}



