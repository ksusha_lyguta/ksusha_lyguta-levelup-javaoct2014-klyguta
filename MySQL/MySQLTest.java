package HomeWork.MySQL;

import java.sql.*;


    /**
     * Created by Ксю on 02.12.2014.
     */
    public class MySQLTest {
        Connection connection;

        public static void main(String[] args) {
            MySQLTest test = new MySQLTest();
            test.getConnection();
            test.execute();
            test.insert();
            test.executePrepared();
            test.closeConnection();


        }

        public void getConnection() {
            try {
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/levelup", "root", "vereschak83");
            } catch (ClassNotFoundException e1) {
                e1.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        public void execute() {
            try {
                Statement statement = connection.createStatement();
                statement.execute("SELECT*FROM student");
                ResultSet resultSet = statement.getResultSet();
                while (resultSet.next()) {
                    Integer id = resultSet.getInt("id");
                    String name = resultSet.getString("Name");
                    Integer age = resultSet.getInt("Age");
                    System.out.println(id + "," + name + "," + age + ",");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        public void insert() {

            PreparedStatement statement = null;
            ResultSet resultSet = null;
            try {
                statement = connection.prepareStatement("insert into student(name, age) values (?,?)");
                statement.setString(1, "Sasha");
                statement.setInt(2, 15);

            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        public void executePrepared() {
            PreparedStatement statement;
            ResultSet resultSet;
            try {

                statement = connection.prepareStatement("SELECT*FROM student where id =? and name =?");
                resultSet = statement.getResultSet();
                while (resultSet.next()) {

                    Integer id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    System.out.println(id + "name is" + name);
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


        public void closeConnection() {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


