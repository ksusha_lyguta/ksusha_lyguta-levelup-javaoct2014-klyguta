package HomeWork.DirScanner;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;


/**
 * Created by Ксю on 20.11.2014.
 * Считывать через сканнер директории. Проверять, существует или нет.
 * После нажатия Enter выводим содержимое отсортированное (список файлов и список папок).
 * Сортировать надо по имени внутри каждой группы (сначала директории, а затем файлы).
 * Если директория, то записывать к ней dir
 */
public class DirScanner {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String f = sc.nextLine();
        if (f.isEmpty()) {
            System.out.println("Try again");
        } else {
            System.out.println(dirScanner((f), new ArrayList<String>()));
        }
    }

    public static ArrayList<String> dirScanner(String s, ArrayList<String> list) {
        File file = new File(s);
        File[] files = file.listFiles();
        for (File f : files) {
            if (f.isDirectory()) {
                list.add(f.getName() + ".dir");
                dirScanner(s + f.separator + f.getName(), list);
            } else {
                list.add(f.getName());
            }
        }
        Sort sort = new Sort();
        Collections.sort(list, sort);
        return list;
    }

}






