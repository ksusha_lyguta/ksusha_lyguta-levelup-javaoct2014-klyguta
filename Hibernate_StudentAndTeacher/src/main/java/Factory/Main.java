package Factory;

import Entities.Student;
import Entities.Subject;
import Entities.Teacher;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by Ksu on 09.06.2015.
 */
public class Main {
    private SessionFactory sessionFactory = null;

    public Main() {
        sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
    }

    private Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    private void closeSessionFactory() {
        sessionFactory.close();
    }

    private void fillDB() {
        getCurrentSession().beginTransaction();
        Subject subj1 = new Subject("математический анализ");
        Subject subj2 = new Subject("алгебра");
        getCurrentSession().save(subj1);
        getCurrentSession().save(subj2);
        Teacher t = new Teacher("Иванов И.И.", "к.ф.-м.н.", "доцент");
        getCurrentSession().save(t);
        Student s = new Student("Сидоров А.Ц", new GregorianCalendar(1988, 3 - 1, 12).getTime(), "адрес 1", "99-22-42", t);
//        s.getSubjects().add(s);
//        s.getSubjects().add(t);
        getCurrentSession().save(s);
        s = new Student("Смирнов З.З.", new GregorianCalendar(1990, 10 - 1, 16).getTime(), null, null, t);
//        s.getSubjects().add(subj2);
        getCurrentSession().save(s);
        getCurrentSession().getTransaction().commit();
    }

    private void modifyObjects() {
        getCurrentSession().beginTransaction();
        Student s = (Student) getCurrentSession().get(Student.class, 1);
        System.out.println("Student" + s.getName());
        if (s.getAdviser() != null) {
            System.out.println("Руководитеь" + s.getAdviser().getName() + ")");
        }
        s.setAdviser(null);
        getCurrentSession().getTransaction().commit();
    }

    @SuppressWarnings("unchecked")
    private void queriesDemo() {
        getCurrentSession().beginTransaction();
        System.out.println("1.Поиск студентов по Ф.И.О.:");
        String studentName = "Сидоров А.Ц";
        Query q = getCurrentSession().createQuery("FROM Student Where name=:studentName");
        q.setParameter("studentName", studentName).list();
        List<Student> result = q.list();
        for (Student s : result) {
            System.out.println(s.getName() + "" + s.getBirthDate() + "" + s.getAdress() + "" + s.getPhone());

            System.out.println("2.Общее количество студентов:");
            q = getCurrentSession().createQuery("SELECT Count (*) FROM Student");
            System.out.println(q.uniqueResult());

            System.out.println("3.Количество студентов у преподавателей");
            q = getCurrentSession().createQuery("SELECT teacher, teacher.students.size" + "FROM Teacher as teacher");
            List<Object[]> result2=q.list();

            }
        }
     public static void main (String[] args){
         Main main = new Main();
         main.fillDB();
         main.queriesDemo();
         main.modifyObjects();
         main.closeSessionFactory();
    }
}
