package Entities;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

/**
 * Created by Ksu on 09.06.2015.
 */
public class Subject implements Serializable {
    private static final long serialVersionUID = 3L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idSubject;
    String name;
    public Subject(){

    }

    public Subject(String name) {
        this.name = name;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
     this.name=name;
    }
    public int getIdSubject(){
        return idSubject;
    }
}
