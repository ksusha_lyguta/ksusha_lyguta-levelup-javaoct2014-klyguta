package Entities;

import javax.persistence.*;
import javax.security.auth.Subject;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Ksu on 09.06.2015.
 */
//Класс для SessionFabric
@Entity
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idStudent;
    private String name;
    private String adress;
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    private String phone;
    @ManyToOne
    @JoinColumn(name = "idTeacher", referencedColumnName = "idTeacher")
    private Teacher adviser;
    @ManyToMany
    @JoinTable(name = "Student_Subject", joinColumns = {@JoinColumn(name = "idSubject")})
    private Set<Subject> subjects;


    public Student(String s, Date time, String s1, String s2, Teacher teacher) {
        subjects = new HashSet<Subject>();
    }

    public Student(String name, String adress, Date birthDate, String phone) {
        this.name = name;
        this.adress = adress;
        this.birthDate = birthDate;
        this.phone = phone;
        subjects = new HashSet<Subject>();
    }

    public Integer getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(Integer idStudent) {
        this.idStudent = idStudent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Set<Subject> getSubjects() {
        return subjects;
    }

    public int hashCode() {
        int hash = 0;
        hash += (idStudent != null ? idStudent.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof Student)) {
            return false;
        }

        Student other = (Student) object;
        if ((this.idStudent == null && other.idStudent != null) || (this.idStudent != null && this.idStudent.equals(other.idStudent))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "entities.Entities.Student[idStudent=" + idStudent + "]";
    }



    public void setSubjects(Set<Subject> subjects) {
        this.subjects = subjects;
    }

    public Teacher getAdviser() {
        return adviser;
    }

    public void setAdviser(Teacher adviser) {
        if (this.adviser != null) {
            this.adviser.getStudentsInternal().remove(this);
        }
        if (adviser != null) {
            adviser.getStudentsInternal().add(this);
        }
        this.adviser = adviser;
    }

}
