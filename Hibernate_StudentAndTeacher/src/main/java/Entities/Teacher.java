package Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collections;
import java.util.Set;


/**
 * Created by Ksu on 09.06.2015.
 */
@Entity
public class Teacher implements Serializable {


    public static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idTeacher", nullable = false)
    private Integer idTeacher;
    private String degree;
    private String name;
    private String post;

    public Teacher(String degree, String name, String post) {
        this.degree = degree;
        this.name = name;
        this.post = post;
    }

    public Integer getIdTeacher() {
        return idTeacher;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }


    @OneToMany(mappedBy = "adviser")
    @OrderBy("name")
    private Set<Student> students;

    public Set<Student> getStudents() {
        return Collections.unmodifiableNavigableSet((java.util.NavigableSet<Student>) students);
    }

    Set<Student> getStudentsInternal() {
        return students;
    }

    public void addStudent(Student s) {
        if (s.getAdviser() != null) {
            s.getAdviser().students.remove(s);
        }
        students.add(s);
        s.setAdviser(this);
    }

    public void removeStudent(Student s) {
        if (students.contains(s)) {
            students.remove(s);
            s.setAdviser(null);
        }

    }
    protected void setStudents(Set<Student> students){
        this.students=students;
    }

    public int hasCode (){
        int hash=0;
        hash += (idTeacher !=null?idTeacher.hashCode():0);
        return hash;
    }
    public boolean equals (Object object){
        if(!(object instanceof Teacher)){
            return false;
        }
        Teacher other = (Teacher)object;
        if ((this.idTeacher==null&&other.idTeacher!=null)||(this.idTeacher!=null&&!this.idTeacher.equals(other.idTeacher))){
            return false;
        }
        return true;
}
public String toString (){
    return name;
    }
}
