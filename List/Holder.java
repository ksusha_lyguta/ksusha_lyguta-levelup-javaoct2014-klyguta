package HomeWork.List;

public class Holder {

    public Holder next;

    public Integer data;

    public Holder(Integer data) {
        this.data = data;
    }

    public void setData(Integer newData) {
        this.data = newData;
    }

}