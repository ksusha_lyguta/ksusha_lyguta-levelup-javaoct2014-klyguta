package HomeWork.List;


import java.util.Iterator;

public class HolderCollection implements Iterable {
    public Holder head = null;
    public int size = 0;

    public void add(Integer data) {
        if (size == 0) {
            head = new Holder(data);
        } else {
            Holder temp = head;
            for (int i = 0; i < size - 1; i++) {
                temp = temp.next;
            }
            temp.next = new Holder(data);
        }
        size++;
    }

    public void add(int x, Integer data) {
        Holder temp = head;
        if (size == 0) {
            head = new Holder(data);
        } else if (x < size) {
            for (int i = 0; i < size - 1; i++) {
                temp = temp.next;
            }
            temp.next = new Holder(data);
            Holder tempAnother = temp.next;
            temp.next.next = tempAnother;
        }
        size++;
    }


    public Integer get(int index) {
        Holder temp = head;
        for (int i = 0; i < size; i++) {
            if (index == i + 1) {
                return temp.data;
            } else {
                temp = temp.next;
            }
        }
        return null;
    }

    public void remove() {
        Holder temp = head;
        if (head == null) {
            return;
        }
        for (int i = 0; i < size - 1; i++) {
            temp = temp.next;
        }
        temp.next = null;
        size--;
    }

    public void remove(int x) {
        Holder temp = head;
        if (size == 0) {
            return;
        } else {
            for (int i = 0; i < x - 1; i++) {
                temp = temp.next;
            }
            temp.next = temp.next.next;
            size--;
        }
    }

    @Override
    public Iterator iterator() {
        return new HolderCollectionIterator();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    private class HolderCollectionIterator implements Iterator {
        int currentPosition;

        @Override
        public boolean hasNext() {
            return currentPosition <= size;
        }

        @Override
        public Integer next() {
            return HolderCollection.this.get(currentPosition++);
        }

        @Override
        public void remove() {

        }
    }
}
