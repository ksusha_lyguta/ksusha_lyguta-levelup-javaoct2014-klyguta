package HomeWork;
import java.io.*;
/**
 * Created by Ксю on 14.11.2014.
 */
public class TextFile {

    public static void main(String[] args) {
        File tempText = new File("d:/Java/LevelUp/src/HomeWork/Temp.txt");
        File result = new File ("d:/Java/LevelUp/src/HomeWork/Result.txt");
        if (tempText.exists()) {
            try (BufferedReader reader = new BufferedReader(new FileReader(tempText))){
                BufferedWriter writer = new BufferedWriter(new FileWriter(result));
                String a1;
                while ((a1 = reader.readLine())!= null){
                   if (a1.contains("Java")){
                       writer.write(a1);
                       writer.newLine();
                   }
                }
                writer.flush();
            }
            catch (IOException e){
                System.out.println("Some trouble");
            }
        }

    }
}