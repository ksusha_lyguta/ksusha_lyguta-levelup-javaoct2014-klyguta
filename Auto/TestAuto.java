package HomeWork.Auto;
import java.util.*;

/**
 * Created by Ксю on 07.11.2014.
 */
public class TestAuto {
    public static void main(String[] args) {
        Auto auto1 = new Auto("Red", "BMW", "10");
        Auto auto2 = new Auto("Green", "Mazda", "100");
        Auto auto3 = new Auto("Blue", "Mazeratti", "1000");
        Auto auto4 = new Auto("Blue", "Toyota", "1000");

        ArrayList<Auto> autos = new ArrayList<Auto>();

        autos.add(auto1);
        autos.add(auto2);
        autos.add(auto3);
        autos.add(auto4);

        System.out.println(autos);
        Collections.sort(autos);
        System.out.println(autos);


        TreeSet<Auto> autosSortByNumber = new TreeSet<Auto>(new Comparator<Auto>() {

            @Override
            public int compare(Auto o1, Auto o2) {
                return o1.number.compareTo(o2.number);
            }
        });
        autosSortByNumber.addAll(autos);
        System.out.println(autosSortByNumber);


        TreeSet<Auto> autosSortByName = new TreeSet<Auto>(new Comparator<Auto>() {
            @Override
            public int compare(Auto o1, Auto o2) {
                return o1.name.compareTo(o2.name);
            }
        });
        autosSortByName.addAll(autos);
        System.out.println(autosSortByName);

        TreeSet<Auto> autoSortByAllParametrs = new TreeSet<Auto>(new Comparator<Auto>() {
            @Override
            public int compare(Auto o1, Auto o2) {
                return o1.name.compareTo(o2.name) & o1.colour.compareTo(o2.colour) & o1.number.compareTo(o2.number);
            }
        });
        autoSortByAllParametrs.addAll(autos);
        System.out.println(autoSortByAllParametrs);
    }
}


