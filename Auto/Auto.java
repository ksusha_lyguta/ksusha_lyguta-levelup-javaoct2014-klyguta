package HomeWork.Auto;

/**
 * Created by Ксю on 07.11.2014.
 */
public class Auto implements Comparable<Auto>{

    public String colour;
    public String name;
    public String number;


 public Auto(String colour, String name, String number){
     this.colour = colour;
     this.name = name;
     this.number = number;
 }



    @Override
    public int compareTo(Auto o) {
        return colour.compareTo(o.colour);
    }

    @Override
    public String toString() {
        return "Auto :" +
                "colour is'" + colour + '\'' +
                ", name is'" + name + '\'' +
                ", number is'" + number;
    }



}
