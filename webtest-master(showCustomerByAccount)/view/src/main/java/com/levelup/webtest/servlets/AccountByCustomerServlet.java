package com.levelup.webtest.servlets;

import com.levelup.model.Account;
import com.levelup.service.AccountService;
import com.levelup.service.impl.AccountServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ksu on 26.02.2015.
 */
public class AccountByCustomerServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String firstName = request.getParameter("firstName");

        if (firstName != null) {
            AccountService accountService = new AccountServiceImpl();
            List<Account> accounts = accountService.getAccountByCustomer(firstName);
            request.setAttribute("accounts",accounts);
            request.getRequestDispatcher("/customer.jsp").forward(request,response);
        }
    }
}

