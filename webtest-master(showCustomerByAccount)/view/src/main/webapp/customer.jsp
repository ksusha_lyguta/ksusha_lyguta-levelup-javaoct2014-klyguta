<%@ page import="com.levelup.service.AccountService" %>
<%@ page import="com.levelup.service.impl.AccountServiceImpl" %>
<%@ page import="com.levelup.model.Account" %>
<%@ page import="java.util.List" %>

<%--
  Created by IntelliJ IDEA.
  User: Ksu
  Date: 26.02.2015
  Time: 2:13
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>



<head>
    <title>List of Accounts by Customer</title>
</head>

<body>


<div class="customer">
    <div>
        Account:
        <div name="accounts">
            <c:forEach var="accounts" items="${requestScope.accounts}">
                <option><c:out value="${account.accountNumber}"/></option>
            </c:forEach>

            <c:if test="${empty accounts}">
                there are no values in accounts
            </c:if>
        </div>

        </div>
    </div>

</div>

</body>
</html>
