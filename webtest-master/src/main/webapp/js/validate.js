

$(document).ready(function(){
    $("#index").on('click',"#Send",function (event){
        event.preventDefault();

        rules: {

            firstName: {
                required: true,
                firstName: true
            }
            lastName: {
                required: true,
                lastName: true
            }
            age: {
                required: true,
                min: 18
            }

        }
        messages: {

            age: {
                required: "Возраст",
                min: "Возраст должен быть больше 18 лет"
            }
        }
    });
});