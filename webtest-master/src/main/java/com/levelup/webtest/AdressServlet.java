package com.levelup.webtest;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ksu on 06.02.2015.
 */
public class AdressServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String adress = request.getParameter("adress");

        RequestDispatcher dispatcher = request.getRequestDispatcher("adress.jsp");

        if(dispatcher!=null){
         request.getSession().setAttribute("adress", adress);
            response.sendRedirect("index.jsp");
        } else {
            response.sendRedirect("adress.jsp");
        }


    }
}
