package com.levelup.webtest;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Ksu on 06.02.2015.
 */
public class FinanceServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        String numberOfCard = request.getParameter("numberOfCard");
        String releaseDate = request.getParameter("releaseDate");
        String cvv = request.getParameter("cvv");
        String secretQuestion = request.getParameter("secretQuestion");
        String secretAnswer = request.getParameter("secretAnswer");

        RequestDispatcher dispatcher = request.getRequestDispatcher("finance.js");
        if (dispatcher != null) {
            request.getSession().setAttribute("numberOfCard", numberOfCard);
            request.getSession().setAttribute("releaseDate", releaseDate);
            request.getSession().setAttribute("cvv", cvv);
            request.getSession().setAttribute("secretQuestion", secretQuestion);
            request.getSession().setAttribute("secretAnswer", secretAnswer);
            response.sendRedirect("adress.jsp");
        } else {
            response.sendRedirect("finance.jsp");
        }
    }
}