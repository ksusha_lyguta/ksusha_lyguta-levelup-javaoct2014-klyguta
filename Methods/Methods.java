package HomeWork.Methods;


/**
 * Created by Ксю on 27.10.2014.
 */
public class Methods {

    public static void main(String[] args) {

        System.out.println(noTeenSum(1, 2, 3));
        System.out.println(noTeenSum(2, 13, 1));
        System.out.println(noTeenSum(2, 1, 14));

        System.out.println(makeBricks(3, 1, 8));
        System.out.println(makeBricks(3, 1, 9));
        System.out.println(makeBricks(3, 2, 10));

        System.out.println(blackjack(19, 21));
        System.out.println(blackjack(21, 19));
        System.out.println(blackjack(19, 22));


    }

    public static boolean makeBricks(int x, int y, int result) {
        if (x + (y * 5) >= result) {
            return true;
        } else return false;
    }

    public static int fixTeen(int a) {
        if (a >= 13 && a <= 19 && a != 16 && a != 15) {
            a = 0;
        }
        return a;
    }

    public static int noTeenSum(int i, int j, int z) {
        return fixTeen(i) + fixTeen(j) + fixTeen(z);
    }

    public static int blackjack(int x, int y) {
        if (x > 21 && y > 21 && x < 0 && y < 0) {
            return 0;
        }
        if (x > y && x > 21) return y;
        if (x > y && x <= 21) return x;
        if (y > x && y > 21) return x;
        if (y > x && y <= 21) return y;
        else return x;

    }
}