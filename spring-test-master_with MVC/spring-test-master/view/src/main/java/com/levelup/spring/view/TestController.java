package com.levelup.spring.view;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
/**
 * Created by Ksu on 19.03.2015.
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/")
    public String addName(Model model){
        model.addAttribute("name","Name Attribute");
        return "test";
    }

    @RequestMapping("/")
    ModelAndView setNameParameter(@RequestParam ("name") String nameParameter){
        ModelAndView parameter = new ModelAndView();
        parameter.addObject("nameParameter",nameParameter);
        parameter.setViewName("test");
        return parameter;
    }
}
