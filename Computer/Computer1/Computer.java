package HomeWork.Computer.Computer1;

/**
 * Created by Ксю on 26.10.2014.
 */
public class Computer {
    private CPU cpu;
    private HDD hdd;
    private RAM ram;
    private Sound sound;
    private Video video;

    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.setCpu(new CPU());
        computer.setRam(new RAM());
        computer.setHdd(new HDD());
        computer.setSound(new Sound());
        computer.setVideo(new Video());
    }

    public void setSound(Sound sound) {
        this.sound = sound;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public void setHdd(HDD hdd) {
        this.hdd = hdd;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }

}
