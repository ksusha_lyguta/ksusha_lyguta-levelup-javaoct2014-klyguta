package HomeWork.Computer.Computer2;

/**
 * Created by Ксю on 26.10.2014.
 */
public class Computer {
   private LCD lcd;
   private MP3 mp3;
   private Toshiba toshiba;
   private DIMM dimm;
   private Celeron celeron;

    public void setLcd(LCD lcd) {
        this.lcd = lcd;
    }

    public void setMp3(MP3 mp3) {
        this.mp3 = mp3;
    }

    public void setToshiba(Toshiba toshiba) {
        this.toshiba = toshiba;
    }

    public void setDimm(DIMM dimm) {
        this.dimm = dimm;
    }

    public void setCeleron(Celeron celeron) {
        this.celeron = celeron;
    }

    Computer (DIMM dimm){
        this.dimm = dimm;
    }
    Computer (LCD lcd, DIMM dimm, Celeron celeron, Toshiba toshiba, MP3 mp3){
        this.dimm = dimm;
        this.celeron = celeron;
        this.lcd = lcd;
        this.toshiba = toshiba;
        this.mp3 = mp3;

    }

    public static void main(String[] args) {
        Computer computer = new Computer (new LCD(), new DIMM(), new Celeron(), new Toshiba(), new MP3());
    }
}
