package HomeWork.Greeting;

/**
 * Created by Ксю on 22.10.2014.
 */
public class TestGreeting {

    static void printGreeting(Greeting greeting) {
        System.out.println(greeting.getGreeting());
        System.out.println(greeting.getGteetingWithName("Ksusha"));
    }

     static class EnglishGreeting implements Greeting {
        @Override
        public String getGreeting() {
            return "Hello";
        }

        @Override
        public String getGteetingWithName(String name) {
            return "Hello " + name;
        }
    }

     static class PortugalGreeting implements Greeting{
         @Override
         public String getGreeting() {
             return "Ola";
         }

         @Override
         public String getGteetingWithName(String name) {
             return "Ola " + name;
         }
     }




    public static void main(String[] args) {

        Greeting englishGreeting = new EnglishGreeting();
        Greeting portugalGreeting = new PortugalGreeting();
        Greeting russianAnonymousGreeting = new Greeting() {
            @Override
            public String getGreeting() {
                return null;
            }

            @Override
            public String getGteetingWithName(String name) {
                return null + name;
            }
        };


        printGreeting(englishGreeting);
        printGreeting(portugalGreeting);
        printGreeting(russianAnonymousGreeting);

    }
}



