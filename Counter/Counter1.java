package HomeWork.Counter;

/**
 * Created by Ксю on 11.12.2014.
 */
public class Counter1 implements Runnable {

    Counter counter;


    public Counter1(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        while (true) {
            if (counter.increment() % 10 == 0) {
                System.out.println(Thread.currentThread().getName());
            }
        }
    }
}
