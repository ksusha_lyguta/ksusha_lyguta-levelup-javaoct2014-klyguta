package com.levelup.spring.view;

import com.levelup.spring.model.dto.CompaniesWrapper;
import com.levelup.spring.model.dto.StockPrice;

import com.levelup.spring.service.PricesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ksu on 27.03.2015.
 */
@Controller
public class CompaniesController {

    @Autowired
    private PricesService pricesService;

    @RequestMapping("/stock")
    public String getStockView(Model model) {
        return "stock";
    }


    @RequestMapping(value = "/getUpdates", method = RequestMethod.GET, produces = "application/json")
    public StockPrice getStockUpdates(@RequestParam("companies") String company) {
        StockPrice price = pricesService.getStockPrice(company);
        return price;
    }

    @RequestMapping(value="company", method=RequestMethod.POST,consumes="application/json",produces="application/json")
    @ResponseBody
    public List<String> saveCompany(@RequestBody CompaniesWrapper wrapper) {
        List<String> response = new ArrayList<String>();
        for (String companies: wrapper.getCompanies()){
            response.add("Saved company: " + companies.toString());
        }
        return response;
    }
}


