package com.levelup.spring.model.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ksu on 27.03.2015.
 */
public class CompaniesWrapper implements Serializable {


    private List<String> companies;

    public List<String> getCompanies() {
        return companies;
    }

    public void setCompanies(List<String> companies) {
        this.companies = companies;
    }
}
