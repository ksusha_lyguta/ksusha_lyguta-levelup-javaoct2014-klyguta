package HomeWork.FileFilter;

import java.io.*;

/**
 * Created by Ксю on 18.11.2014.
 */
public class CopyByFileFilter {

    // метод для копирования файлов
    public static void copyDirectory(String startDir, String dirWithImage) throws IOException {

        File source = new File(startDir);
        File target = new File(dirWithImage);
        FilenameFilter image = new FileFilterImage();
        File[] list = source.listFiles(image);
        target.mkdir();
        for (File f : list) {
            // если директория, копируем эту папку
            if (f.isDirectory()) {
                copyDirectory(f.getAbsolutePath(), "d://withImage" + "//" + f.getName());
                // если просто файл, копируем сам файл
            } else if (f.isFile()) {
                BufferedInputStream inside = new BufferedInputStream(new FileInputStream(source));
                BufferedOutputStream outside = new BufferedOutputStream(new FileOutputStream(target));
                byte[] name = source.getName().getBytes();
                while ((inside.read(name)) != -1) {
                    outside.write(name);
                }
                inside.close();
                outside.flush();
                outside.close();
            }

        }
    }

}