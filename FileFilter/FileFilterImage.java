package HomeWork.FileFilter;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by Ксю on 18.11.2014.
 */
public class FileFilterImage implements FilenameFilter {
    @Override
    public boolean accept(File dir, String name) {
        // фильтруем по названию image
        if (name.contains("image")) {
            return name.contains("image");
        }
        return false;
    }
}
